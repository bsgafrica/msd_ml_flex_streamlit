import streamlit as st 
import numpy as np 
import pandas as pd
import datetime
import matplotlib.pyplot as plt
    
from ml.scoring_script import score_model


# movies_path = 'ml/data/mytest_movies.csv'
title_principal_path = 'ml/data/mytest_title_pricipal.csv'
model_filename = 'ml/data/model/final_rf_model.pkl'


movies = pd.read_csv('data/mytest_movies.csv')
#Keep first row only
movies = movies.drop( movies.index.to_list()[1:] ,axis = 0 )
#title_principal = pd.read_csv(title_principal_path)
#must have more than one record
#title_principal = title_principal.drop( title_principal.index.to_list()[6:] ,axis = 0 )

def run_model(genres,actors,date,txt):
    
    # st.write('Genres:', ', '.join(genres))
    # st.write('Actors:', ', '.join(actors))
    # st.write('Release date:', date)
    # st.write('Movie description:', txt)
    
    #Create an empty df    
    local_title_principal = pd.DataFrame(columns=['imdb_title_id','category','imdb_name_id'])
    

    if actors:
        movies['actors'][0] =  ', '.join(actors)
    if genres:
        movies['genre'][0] =  ', '.join(genres)
    if date:
        movies['date_published'][0] = str(date)
    if txt:
        movies['description'][0] = str(txt)
    if txt_duration:
        movies['duration'][0] = str(txt_duration)

    temp = list_of_prod_companies[list_of_prod_companies['production_company'] == prod_company]['production_company']
    temp = temp.iloc[0]
    movies['production_company'][0] =  str(temp)
    
    # st.dataframe(movies[['title','date_published', 'genre']].head(50))
    #st.dataframe(movies[:1])
    

    #Director will always be position 0
    temp= ''
    #title_principal['category'][0] = 'director'
    #find the person
    temp = list_of_director[list_of_director['name'] == director]['imdb_name_id']
    temp = temp.iloc[0]
    print(temp)
    #title_principal['imdb_name_id'][0] = str(temp)
    
    local_title_principal = local_title_principal.append(
        {'imdb_title_id':'new_movie'
            ,'category':'director'
            ,'imdb_name_id':temp
            }, ignore_index = True
        )
    
    #Composer will always be position 1
    temp= ''
    #find the person
    temp = list_of_composer[list_of_composer['name'] == composer]['imdb_name_id']
    temp = temp.iloc[0]

    local_title_principal = local_title_principal.append(
    {'imdb_title_id':'new_movie'
        ,'category':'composer'
        ,'imdb_name_id':temp
        }, ignore_index = True
    )


    #Writer will always be position 2
    temp= ''
    #find the person
    temp = list_of_writer[list_of_writer['name'] == writer]['imdb_name_id']
    temp = temp.iloc[0]

    local_title_principal = local_title_principal.append(
    {'imdb_title_id':'new_movie'
        ,'category':'writer'
        ,'imdb_name_id':temp
        }, ignore_index = True
    )


    
    #cinematographer will always be position 3
    temp= ''
    #find the person
    temp = list_of_cinematographer[list_of_cinematographer['name'] == cinematographer]['imdb_name_id']
    temp = temp.iloc[0]
    
    local_title_principal = local_title_principal.append(
    {'imdb_title_id':'new_movie'
        ,'category':'cinematographer'
        ,'imdb_name_id':temp
        }, ignore_index = True
    )    


    #producer will always be position 4
    temp= ''
    #find the person
    temp = list_of_producer[list_of_producer['name'] == producer]['imdb_name_id']
    temp = temp.iloc[0]
    
    local_title_principal = local_title_principal.append(
    {'imdb_title_id':'new_movie'
        ,'category':'producer'
        ,'imdb_name_id':temp
        }, ignore_index = True
    )        
    
    #add the actors
    for i in range(len(actors)):
        #print('actor: ' + str(i))
        temp= ''
        #find the person
        temp = list_of_actors[list_of_actors['name'] == actors[i]]['imdb_name_id']
        temp = temp.iloc[0]
        
        #title_principal[last_index+i]['imdb_title_id'] = 'new_movie'
       
        local_title_principal = local_title_principal.append({
            'imdb_title_id':'new_movie'
            ,'category':'actor'
            ,'imdb_name_id':str(temp)}, ignore_index = True)
        
   
    #add the actresses
    for i in range(len(actress)):
        #print('actress: ' + str(i))
        temp= ''
        #find the person
        temp = list_of_actress[list_of_actress['name'] == actress[i]]['imdb_name_id']
        temp = temp.iloc[0]
        #title_principal[last_index+i]['imdb_title_id'] = 'new_movie'
       
        local_title_principal = local_title_principal.append({
            'imdb_title_id':'new_movie'
            ,'category':'actress'
            ,'imdb_name_id':str(temp)}, ignore_index = True)
                
        


    #print out title principle df
    #st.dataframe(local_title_principal)
    
    #write out the data to a file
    local_title_principal.to_csv('ml/data/real_title_principal.csv', index = False)
    title_principal_path = 'ml/data/real_title_principal.csv'    
    z = score_model(movies,title_principal_path,model_filename)
    
    #print dataframe for testing
    #st.dataframe(z[:1])

    prediction = float(z['Prediction'][0])
    if prediction == 1:
        probability = float(z['Probability_1'][0])
        # probability = probability*100
        st.success("You will get a rating of 6.5 or above with a probability of: {:.0%}".format(probability))
    else:
        probability = float(z['Probability_0'][0])
        # probability = probability*100
        st.error("You will get a rating below 6.5 with a probability of: {:.0%}".format(probability))

    # fig1, ax1 = plt.subplots()
    # plt.style.use('dark_background')
    # ax1.pie([z['Probability_1'],z['Probability_0']], labels=['Probability_1', 'Probability_0']
    #         ,autopct='%1.1f%%', startangle=90)
    # ax1.axis('equal')  # Equal aspect ratio ensures that pie is drawn as a circle.
    # st.pyplot(fig1)
    
    

st.title("How well will your movie do?")
with st.form(key='my_form'):
    ##########################################
    ################# Genres #################
    ##########################################
    
    genre_options = [
                'Action',
                'Adult',
                'Adventure',
                'Animation',
                'Biography',
                'Comedy',
                'Crime',
                'Documentary',
                'Drama',
                'Family',
                'Fantasy',
                'Film-Noir',
                'History',
                'Horror',
                'Music',
                'Musical',
                'Mystery',
                'Romance',
                'Sci-Fi',
                'Sport',
                'Thriller',
                'War',
                'Western',
                'Reality-TV'
           ]
    
    genres = st.multiselect(
    	     'Which Genre\'s do you want your movie to be (max 3)?',genre_options)
    
    #####################################################
    ################# Duration #################
    #####################################################
    
    txt_duration = st.number_input('Movie duration (mins)',min_value =1, max_value=500)
    
    
    ##########################################
    ################# Actors + Cast #################
    ##########################################
    list_of_actors = pd.read_csv('ml/data/list_of_actors.csv')
    actor_options = list_of_actors['name']
    
    list_of_actress = pd.read_csv('ml/data/list_of_actress.csv')
    actress_options = list_of_actress['name']
    
    list_of_writer = pd.read_csv('ml/data/list_of_writer.csv')
    writer_options = list_of_writer['name']
    
    list_of_cinematographer = pd.read_csv('ml/data/list_of_cinematographer.csv')
    cinematographer_options = list_of_cinematographer['name']
    
    list_of_composer = pd.read_csv('ml/data/list_of_composer.csv')
    composer_options = list_of_composer['name']
    
    list_of_director = pd.read_csv('ml/data/list_of_director.csv')
    director_options = list_of_director['name']
    
    list_of_producer = pd.read_csv('ml/data/list_of_producer.csv')
    producer_options = list_of_producer['name']

    list_of_prod_companies = pd.read_csv('ml/data/list_of_prod_companies.csv')
    prod_company_options = list_of_prod_companies['production_company']
    
    
    actors = st.multiselect(
    	     'Which actors do you want in your movie?',actor_options)
    
    actress = st.multiselect(
    	     'Which actresses do you want in your movie?',actress_options)
    
    prod_company = st.selectbox(
	     'Which production company is producing the movie?',prod_company_options)
    
    director = st.selectbox(
    	     'Who is the director for your movie?',director_options)    
    
    producer = st.selectbox(
    	     'Who is the producer for your movie?',producer_options)
    
    writer = st.selectbox(
    	     'Who is the writer for your movie?',writer_options)
    
    cinematographer = st.selectbox(
    	     'Who is the cinematographer for your movie?',cinematographer_options)
    
    composer = st.selectbox(
    	     'Who is the composer for your movie?',composer_options)
    

    

    
    
    ################################################
    ################# Release date #################
    ################################################
    
    date = st.date_input('Release date', datetime.date(2021,11,12))
    
    
    #####################################################
    ################# Movie description #################
    #####################################################
    
    #txt = st.text_area('Movie description')
    txt = ''
    


    
    #####################################################
    ################# Predict button#################
    #####################################################

    if st.form_submit_button("Predict"): 
        run_model(genres,actors,date,txt)
        #st.success('Your loan is {}'.format(result))
        #print(LoanAmount)
        
    
    



