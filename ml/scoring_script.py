# -*- coding: utf-8 -*-
"""
Created on Wed Nov  3 11:36:26 2021

@author: Jaymal.Kalidas
"""

import pandas as pd
import numpy as np
from .functions.reduce_levels import reduce_levels_to_top_N
from .functions.reduce_levels import apply_levels_to_top_N

from .functions.actors_feature import apply_impact_actor_feature
from .functions.actors_feature import apply_feature_growing_star
from .functions.actors_feature import feature_actor_actress_ratio
from .functions.actors_feature import feature_cast_size
from .functions.actors_feature import apply_title_principal_category

from .functions.addtional_features import apply_coefficient_of_variation_by_genre
from .functions.addtional_features import median_best_vote

from .functions import defsV2

from .functions import topic_modelling_function as tp

import pickle

from sklearn.preprocessing import OneHotEncoder
from joblib import dump, load

from gensim.test.utils import datapath
import gensim
from gensim.corpora import Dictionary




# def score_model(movies_path,title_principal_path, model_filename):
def score_model(movies,title_principal_path, model_filename):
    
    print('Reading data')
    # movies = pd.read_csv(movies_path)
    movies_small = movies[['imdb_title_id','title','year','budget','duration','writer'
                           ,'director','production_company'
                           ,'actors','avg_vote','votes','country','language','genre'
                           ,'date_published','description']]
    
    
    
    # load the model from disk
    #filename = 'data/model/final_rf_model.pkl'
    loaded_model = pickle.load(open(model_filename, 'rb'))
    #test model loaded
    #result = loaded_model.score(x_train, y_train)
    #result 
    
    #drop actor col for now
    movies_small = movies_small.drop(columns ='actors')
    movies_small = movies_small.drop(columns ='title')
    
    ####Basic cleaning
    movies_small['year'] = pd.to_numeric(movies_small['year'], errors = 'coerce')
    
    #drop the budget col - too many nans
    movies_small = movies_small.drop(columns ='budget')
    movies_small['genre_combo'] = movies_small['genre']
    
    
    ######################Features built on all data###############
    print('Building features')
    #Reduced to top n
    input_col_name = 'genre_combo'
    filename = './ml/data/model/reduced'+input_col_name+'.csv' 
    reduceddf =  pd.read_csv(filename)
    reduced_series = reduceddf[input_col_name]
    movies_small = apply_levels_to_top_N(movies_small, input_col_name, reduced_series)
    
    input_col_name = 'writer'
    filename = './ml/data/model/reduced'+input_col_name+'.csv' 
    reduceddf =  pd.read_csv(filename)
    reduced_series = reduceddf[input_col_name]
    movies_small = apply_levels_to_top_N(movies_small, input_col_name, reduced_series)
    
    input_col_name = 'director'
    filename = './ml/data/model/reduced'+input_col_name+'.csv' 
    reduceddf =  pd.read_csv(filename)
    reduced_series = reduceddf[input_col_name]
    movies_small = apply_levels_to_top_N(movies_small, input_col_name, reduced_series)
    
    input_col_name = 'country'
    filename = './ml/data/model/reduced'+input_col_name+'.csv' 
    reduceddf =  pd.read_csv(filename)
    reduced_series = reduceddf[input_col_name]
    movies_small = apply_levels_to_top_N(movies_small, input_col_name, reduced_series)
    
    input_col_name = 'language'
    filename = './ml/data/model/reduced'+input_col_name+'.csv' 
    reduceddf =  pd.read_csv(filename)
    reduced_series = reduceddf[input_col_name]
    movies_small = apply_levels_to_top_N(movies_small, input_col_name, reduced_series)
    
    
    movies_small['date_published'] = pd.to_datetime(movies_small['date_published'],errors ='coerce')
    movies_small['month'] = movies_small['date_published'].dt.month
    movies_small['day'] = movies_small['date_published'].dt.day
    
    ##Transform to one hot encoded variables
    
    features_to_encode = ['genre_combo','writer','director','country','language']
    ohe = load('./ml/data/model/ohe.joblib') # load and reuse the model
    cat_ohe = ohe.transform(movies_small[features_to_encode])
    column_name = ohe.get_feature_names(['genre_combo','writer','director'
                                         ,'country','language'])
    cat_ohe =  pd.DataFrame(cat_ohe, columns= column_name)
    movies_small = pd.concat([movies_small,cat_ohe],axis=1)
    
    ##Actor acress ratio
    title_principal_path = title_principal_path
    actor_actress = feature_actor_actress_ratio(title_principal_path)
    #actor_actress.to_csv('data/model/actor_actress.csv', index = False)
    #actor_actress = pd.read_csv('data/model/actor_actress.csv')
    movies_small = pd.merge(movies_small,actor_actress,
                            on='imdb_title_id'
                            ,how='left')
    
    movies_small = movies_small.fillna({'actor_ratio':0
                                        ,'only_actors':0
                                        ,'only_actress':0})
    
    ##Cast size
    title_principal_path = title_principal_path
    cast_size = feature_cast_size(title_principal_path)
    #cast_size.to_csv('data/model/cast_size.csv', index = False)
    #cast_size = pd.read_csv('data/model/cast_size.csv')
    movies_small = pd.merge(movies_small,cast_size,
                        on='imdb_title_id'
                        ,how='left')
    
    movies_small = movies_small.fillna({'cast_size':0})
    
    
    ######################Features built on all data###############
    
    x_train = movies_small 
    ######################Features built on training data only###############
    ##Join Actors feature
    feature_impact_actor = pd.read_csv('./ml/data/model/feature_impact_actor_group.csv')
    title_principal_path = title_principal_path
    feature_impact_actor = apply_impact_actor_feature(feature_impact_actor, title_principal_path)
    
    x_train = pd.merge(x_train,feature_impact_actor,
                            on='imdb_title_id'
                            ,how='left')
    x_train = x_train.fillna({'experiance':0
                              ,'impact_actor_high':0
                              ,'impact_actor_medium':0
                              ,'impact_actor_low':0})
    
    ## join improvement indicator
    upcoming_actor = pd.read_csv('./ml/data/model/upcoming_actor.csv')
    feature_upcoming_actor = apply_feature_growing_star(title_principal_path, upcoming_actor)
    x_train = pd.merge(x_train,feature_upcoming_actor,
                            on='imdb_title_id'
                            ,how='left')
    
    x_train = x_train.fillna({'improvement_indicator':0})
    
    ## join relationship feature
    df_relationship_feature = pd.read_csv('./ml/data/relationship_feature.csv')
    df_relationship_feature = df_relationship_feature.drop(columns = 'Unnamed: 0')
    df_relationship_feature = df_relationship_feature.rename(columns={'0':'relationship_strength' })
    
    actor_poi = df_relationship_feature['imdb_name_id']
    actor_poi = actor_poi.drop_duplicates()
    
    #Reduce data to people that we have relationship info about
    title_principal_path = title_principal_path
    title_principal = pd.read_csv(title_principal_path)
    
    results =[]  
    #get movie combinations
    end = len(title_principal['imdb_name_id'])
    for i in range(end):
        if i % 100 == 0:
            print(i)
        results.append(defsV2.apply_person_movie_combinations(i, title_principal))
    results_df = pd.concat(results)
    results_df = pd.merge(results_df,df_relationship_feature,
                  on=['imdb_name_id','person']
                  ,how='left')
    
    results_df = results_df.fillna({'relationship_strength':0})
    results_df = results_df.groupby('imdb_title_id').sum()
    x_train = pd.merge(x_train,results_df,
                            on='imdb_title_id'
                            ,how='left')
    x_train = x_train.fillna({'relationship_strength':0})
    
    
    ## join principal cat average
    ## drop cols
    x_train = x_train.drop(columns ='director')
    x_train = x_train.drop(columns ='writer')
    
    title_principal_path = title_principal_path
    title_principal_average_rating = pd.read_csv('./ml/data/model/title_principal_average_rating.csv')
    title_principal_average_rating = apply_title_principal_category(title_principal_path, title_principal_average_rating)
    
    x_train = pd.merge(x_train,title_principal_average_rating,
                            on='imdb_title_id'
                            ,how='left')
    
    
    ## Join prod company feature
    relevant_prod_companies = pd.read_csv('./ml/data/model/relevant_prod_companies.csv')
    x_train = pd.merge(x_train,relevant_prod_companies,
                            on='production_company'
                            ,how='left')
    x_train = x_train.fillna({'prod_movie_count':0})
    x_train = x_train.fillna({'production_avg_vote':0})
    
    
    growing_prod_companies = pd.read_csv('./ml/data/model/growing_prod_companies.csv')
    
    x_train = pd.merge(x_train,growing_prod_companies,
                            on='production_company'
                            ,how='left')
    x_train = x_train.fillna({'feature_rising_prod_comp':0})
    
    
    
    ##join genre feature
    genre_ratings = pd.read_csv('./ml/data/model/genre_ratings.csv')
    movie_rating = apply_coefficient_of_variation_by_genre(x_train,genre_ratings)
    
    x_train = pd.merge(x_train,movie_rating,
                            on='imdb_title_id'
                            ,how='left')
    
    
    
    ##join month feature
    monthly_median = pd.read_csv('./ml/data/model/monthly_median.csv')
    monthly_median = monthly_median[['month','median_best_vote']]
    x_train = pd.merge(x_train,monthly_median,
                            on='month'
                            ,how='left')
    
    x_train = x_train.drop(columns=('year'))
    x_train = x_train.drop(columns=['date_published'])
    
    x_train= x_train.fillna(0)
    ######################Features built on training data only###############
    
    
    
    ######################Topic modelling###############
    
    
    # Load a pretrained topic model from disk.
    lda_model = gensim.models.ldamodel.LdaModel.load('./ml/data/tm/model')
    dictionary = Dictionary.load('./ml/data/tm/dictionary.gensim')
    
    
    
    
    #print(str(x_train.shape))
    x_train['tokens'] = x_train['description'].apply(tp.tokenize)
    x_train['tokens'] = x_train['tokens'].apply(tp.lemmatize)
    x_train['ngrams'] = x_train['tokens'].apply(tp.make_ngrams, ngram_no=2)
    #print(str(x_train.shape))
    
    
    data = x_train
    #print(str(data.shape))
    unique_identifier = 'imdb_title_id'
    id2word = dictionary
    df_topics = pd.DataFrame()
    for i in range(len(data)):
        
        #for i in range(50000,50005):
        if i % 100 == 0:
            print(f'Progress: {(i+1)} of {len(data)} records processed')
        text = data['ngrams'].iloc[i]
        unique_id = data[unique_identifier].iloc[i]
        
        bow = id2word.doc2bow(text)
        #must update this with the infer_topics
        #df = pd.DataFrame(lda_model.get_document_topics(bow)).transpose().tail(1)
        df = pd.DataFrame(lda_model.get_document_topics(bow),columns=['Topic','Prob']).transpose()
        df = pd.DataFrame(data=[list(df.iloc[1])],columns=list(df.iloc[0].astype(int)))
        
        #df[unique_identifier] = data[unique_identifier].iloc[i]
        df = df.assign(**{unique_identifier: unique_id})
        df_topics = pd.concat([df_topics,df],axis=0)
        
    # Find most likely Topic and probability for each document
    #print('Apply topic probability filtering and finding top matches ...')
    df_topics = df_topics.reset_index()
    df_topics = df_topics.drop(columns=['index'])
    df_topics['Best Topic'] = df_topics[df_topics.columns[~df_topics.columns.isin([unique_identifier,'Best Topic','Best Topic Probability'])]].idxmax(axis=1)
    df_topics['Best Topic Probability'] = df_topics[df_topics.columns[~df_topics.columns.isin([unique_identifier,'Best Topic','Best Topic Probability'])]].max(axis=1)
     
    #print(str(data.shape))    
    data = pd.merge(left=data, right=df_topics, how="left",on=unique_identifier)
    #print(str(data.shape))   
         
    x_train = data   
    
    
    ######################Topic modelling###############
    
    ######################Prep for prediction###############
    ## Fill all nas with zero
    x_train = x_train.fillna(0)
    
    ##Drop cols that make up the target and id fields
    x_train = x_train.drop(columns =['votes','avg_vote','imdb_title_id'
                                     ,'description'
                                     ,'genre'
                                     #,'ngrams','tokens'
                                     ])
    x_train = x_train.drop(columns =['ngrams','tokens'
                                     ])
    
    
    x_train = x_train.drop(columns =['production_company','country','language','genre_combo'])

    ######################Prep for prediction###############
                                     
    ######################Prediction###############

    print('Predicting')
    
    feature_scores = pd.read_csv('./ml/data/model/feature_scores.csv')
    feature_scores = feature_scores['Feature_Name']
    x_train = x_train[feature_scores]
    
    y_pred = loaded_model.predict(x_train)
    y_pred_probs = loaded_model.predict_proba(x_train)
    y_pred_probs= pd.DataFrame(y_pred_probs)
    
    ## join probabilities and prediction back in
    x_train['Prediction'] = y_pred
    x_train['Probability_0'] = y_pred_probs[0]
    x_train['Probability_1'] = y_pred_probs[1]
    x_train['imdb_title_id'] = movies['imdb_title_id']
    print('Complete')
    ######################Prediction###############
    
    
    return(x_train)

# movies_path = 'data/mytest_movies.csv'
# title_principal_path = 'data/mytest_title_pricipal.csv'
# model_filename = 'data/model/final_rf_model.pkl'

# z = score_model(movies_path,title_principal_path,model_filename)

