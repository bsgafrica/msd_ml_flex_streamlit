# -*- coding: utf-8 -*-
"""
Created on Wed Nov  3 10:48:50 2021

@author: Jaymal.Kalidas
"""

##############################CLEAN DATA##############################
movies = pd.read_csv('data/IMDb movies.csv')
movies_small = movies[['imdb_title_id','title','year','budget','duration','writer'
                       ,'director','production_company'
                       ,'actors','avg_vote','votes','country','language','genre'
                       ,'date_published','description']]


#working with good volume of votes 
movies_small = movies_small[movies_small['votes'] > 200]

#####Define target - avg votes >=7 (better balance than 8)
movies_small['Target'] = np.where(
    movies_small['avg_vote'] >= 6.5 , 1, 0)
movies_small['Target'].value_counts()
### Class is imbalanced

#drop actor col for now
movies_small = movies_small.drop(columns ='actors')
movies_small = movies_small.drop(columns ='title')

####Basic cleaning
movies_small['year'] = pd.to_numeric(movies_small['year'], errors = 'coerce')

#drop the budget col - too many nans
movies_small = movies_small.drop(columns ='budget')

##############################Features##############################

movies_small['genre_combo'] = movies_small['genre']
movies_small = reduce_levels_to_top_N(movies_small, 'genre_combo', 5)
movies_small = reduce_levels_to_top_N(movies_small, 'writer', 5)
movies_small = reduce_levels_to_top_N(movies_small, 'director', 5)
#movies_small = reduce_levels_to_top_N(movies_small, 'production_company', 20)
#movies_small = reduce_levels_to_top_N(movies_small, 'actors', 100)
movies_small = reduce_levels_to_top_N(movies_small, 'country', 5)
movies_small = reduce_levels_to_top_N(movies_small, 'language', 5)

#label encode
features_to_encode = ['genre_combo','writer','director','country','language']
#genre
#production_company
#features_to_encode = ['genre','country','language']
movies_small = pd.get_dummies(movies_small, columns = features_to_encode)



##############################Features##############################




##Sort ascending the data by date
movies_small = movies_small.sort_values(by=['date_published'])

#Split train and test
x_train, x_test= np.split(movies_small, [int(.70 *len(movies_small))])


y_train = x_train['Target']
y_test = x_test['Target']

x_train = x_train.drop(columns=['Target'])
x_test = x_test.drop(columns=['Target'])


##Drop cols that make up the target and id fields
x_train = x_train.drop(columns =['votes','avg_vote','imdb_title_id'
                                 ,'description'
                                 ,'genre'
                                 ,'date_published'
                                 ,'year'
                                 ,'production_company'
                                 ])


x_test = x_test.drop(columns =['votes','avg_vote','imdb_title_id'
                                 ,'description'
                                 ,'genre'
                                 ,'date_published'
                                 ,'year'
                                 ,'production_company'
                                 ])

########################MODEL##########################




#Random forest
rf_classifier = RandomForestClassifier(
                      min_samples_leaf=30,
                      n_estimators=300,
                      bootstrap=True,
                      #oob_score=True,
                      #n_jobs=-1,
                      class_weight = 'balanced',
                      random_state=123,
                      max_features='auto')

#Train
rf_classifier.fit(x_train, y_train)

#Predict
y_pred = rf_classifier.predict(x_test)
y_pred_probs = rf_classifier.predict_proba(x_test)


##############################Accuracy##############################

#training score
rf_classifier.score(x_train, y_train)

cm = confusion_matrix(y_test, y_pred)

plot_confusion_matrix(cm  = cm, 
                      normalize    = False,
                      target_names = ['0', '1'],
                      title        = "Confusion Matrix")


##Accuracy stats
from sklearn.metrics import accuracy_score
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from sklearn.metrics import f1_score

print('accuracy: ' + str( metrics.accuracy_score(y_test, y_pred)))
print('precision_score: ' + str(precision_score(y_test, y_pred)))
print('recall_score: ' + str(recall_score(y_test, y_pred)))
print('f1_score: ' + str(f1_score(y_test, y_pred)))

# get importance
importance = rf_classifier.feature_importances_
importance_df = pd.DataFrame(importance)
importance_df = importance_df.set_index( x_train.columns).reset_index()
importance_df =importance_df.rename(columns={'index':'feature'})
