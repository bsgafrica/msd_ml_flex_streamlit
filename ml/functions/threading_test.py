# -*- coding: utf-8 -*-
"""
Created on Thu Oct 21 13:08:24 2021

@author: Jaymal.Kalidas
"""
# Python program to illustrate the concept
# of threading
# importing the threading module
import threading
  
def print_cube(num):
    """
    function to print cube of given num
    """
    print("Cube: {}".format(num * num * num))
  
def print_square(num):
    """
    function to print square of given num
    """
    print("Square: {}".format(num * num))
 
    
title_principal_poi_sample = title_principal_poi
title_principal_poi_sample = title_principal_poi_sample[0:1000]
title_principal_poi_sample = title_principal_poi_sample.reset_index()

end = len(title_principal_poi_sample['imdb_name_id'])

from joblib import Parallel, delayed

 
if __name__ == "__main__":
    # creating thread
    t1 = threading.Thread(target=print_square, args=(10,))
    t2 = threading.Thread(target=print_cube, args=(10,))
  
    # starting thread 1
    t1.start()
    # starting thread 2
    t2.start()
  
    # wait until thread 1 is completely executed
    t1.join()
    # wait until thread 2 is completely executed
    t2.join()
  
    # both threads completely executed
    print("Done!")
    
    
    
    
import numpy as np
np.random.RandomState(100)
arr = np.random.randint(0, 10, size=[200000, 5])
data = arr.tolist()
data[:5]

def howmany_within_range(row, minimum, maximum):
    """Returns how many numbers lie within `maximum` and `minimum` in a given `row`"""
    count = 0
    for n in row:
        if minimum <= n <= maximum:
            count = count + 1
    return count

results = []
for row in data:
    results.append(howmany_within_range(row, minimum=4, maximum=8))

print(results[:10])   
    
 
    
 
    
 
    
 
    
 #https://www.machinelearningplus.com/python/parallel-processing-python/
    
title_principal_poi_sample = title_principal_poi
title_principal_poi_sample = title_principal_poi_sample[0:1000]
title_principal_poi_sample = title_principal_poi_sample.reset_index()

temp_title_principal_name =title_principal.set_index('imdb_name_id')
temp_title_principal_movie =title_principal.set_index('imdb_title_id')
end = len(title_principal_poi_sample['imdb_name_id'])


results = []
i = 0
 
def get_person_relations(i):
    current_person = title_principal_poi_sample['imdb_name_id'][i]    
    person = temp_title_principal_name.loc[current_person]
    #step 2
    person_movie = temp_title_principal_movie.loc[person['imdb_title_id']]
    person_movie = person_movie[person_movie['imdb_name_id'] != current_person]
    person_movie['person'] = current_person

    return person_movie
 
     


starttime = pd.Timestamp.now()  
for i in range(end):
    results.append(get_person_relations(i))
endtime = pd.Timestamp.now()
print(str(endtime-starttime))


import import multiprocess as mp
print("Number of processors: ", mp.cpu_count())

# Step 1: Init multiprocessing.Pool()
pool = mp.Pool(mp.cpu_count())

# Step 2: `pool.apply` the `howmany_within_range()`
results = pool.map(get_person_relations, [i for i in range(end)])

# Step 3: Don't forget to close
pool.close()    

print(results[:10])
    




















    
def process(i):
    
    for i in row:
        current_person = title_principal_poi_sample['imdb_name_id'][i]
        #step 1
        person = temp_title_principal_name.loc[current_person]
        #step 2
        person_movie = temp_title_principal_movie.loc[person['imdb_title_id']]
        person_movie = person_movie[person_movie['imdb_name_id'] != current_person]
        person_movie['person'] = current_person
        
        if i % 100 == 0:
            print(i)
        return person_movie
results = []
for row in data:
    results.append(howmany_within_range(row, minimum=4, maximum=8))
    
    
    
    
import multiprocess as mp
print("Number of processors: ", mp.cpu_count())

# Step 1: Init multiprocessing.Pool()
pool = mp.Pool(mp.cpu_count())

# Step 2: `pool.apply` the `howmany_within_range()`
results = [pool.apply(howmany_within_range, args=(row, 4, 8)) for row in data]

# Step 3: Don't forget to close
pool.close()    

print(results[:10])
    
    
    