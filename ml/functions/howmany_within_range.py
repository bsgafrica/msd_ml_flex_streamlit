# -*- coding: utf-8 -*-
"""
Created on Thu Oct 21 15:26:21 2021

@author: Jaymal.Kalidas
"""
def howmany_within_range(row, minimum, maximum):
    """Returns how many numbers lie within `maximum` and `minimum` in a given `row`"""
    count = 0
    for n in row:
        if minimum <= n <= maximum:
            count = count + 1
    return count