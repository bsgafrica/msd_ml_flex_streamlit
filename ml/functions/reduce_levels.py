# -*- coding: utf-8 -*-
"""
Created on Thu Oct  7 11:15:46 2021

@author: Jaymal.Kalidas
"""
import pandas as pd
import numpy as np

def reduce_levels_to_top_N (df, input_col_name, N):
    """Create a boolean feature based on nulls
    :param df: Input dataframe
    :type df: pandas dataframe
    :param input_col_name: The input column name to reduce levels on
    :type input_col_name: string
    :param output_col_name: The input column name which has nulls
    :type output_col_name: string
    :param N: value count threshold
    :type N: int
    :returns: df -- dataframe
    """
    
    df2 = df[input_col_name].value_counts().reset_index()
    df2.columns = [input_col_name, 'Value']
    df2 = df2.head(N)
    
    filename = './data/model/reduced'+input_col_name+'.csv' 
    df2.to_csv(filename)
    
    df[input_col_name] = np.where(
        df[input_col_name].isin(df2[input_col_name])
                                   ,df[input_col_name],'other' )
    
    print(df[input_col_name].value_counts())    
        
    
    return df


def apply_levels_to_top_N (df, input_col_name,series_top_n):
    """Create a boolean feature based on nulls
    :param df: Input dataframe
    :type df: pandas dataframe
    :param input_col_name: The input column name to reduce levels on
    :type input_col_name: string
    :param output_col_name: The input column name which has nulls
    :type output_col_name: string
    :param N: value count threshold
    :type N: int
    :returns: df -- dataframe
    """
       
    df[input_col_name] = np.where(
        df[input_col_name].isin(series_top_n)
                                   ,df[input_col_name],'other' )
           
    
    return df