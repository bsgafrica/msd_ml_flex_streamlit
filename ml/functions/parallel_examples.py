# -*- coding: utf-8 -*-
"""
Created on Thu Oct 21 14:54:43 2021

@author: Jaymal.Kalidas
"""

import multiprocessing as mp
import random
import string

def cube(x):
    return x**3

pool = mp.Pool(processes=4)

results = [pool.apply(cube, args=(x,)) for x in range(1,7)]

print(results)




#####Example 2

import numpy as np
np.random.RandomState(100)
arr = np.random.randint(0, 10, size=[200000, 5])
data = arr.tolist()
data[:5]




def howmany_within_range(row, minimum, maximum):
    """Returns how many numbers lie within `maximum` and `minimum` in a given `row`"""
    count = 0
    for n in row:
        if minimum <= n <= maximum:
            count = count + 1
    return count


results = []
for row in data:
    results.append(howmany_within_range(row, minimum=4, maximum=8))



    
    