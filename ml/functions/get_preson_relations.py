+# -*- coding: utf-8 -*-
"""
Created on Thu Oct 21 14:49:08 2021

@author: Jaymal.Kalidas
"""
  
def get_person_relations(i):
    current_person = title_principal_poi_sample['imdb_name_id'][i]    
    person = temp_title_principal_name.loc[current_person]
    #step 2
    person_movie = temp_title_principal_movie.loc[person['imdb_title_id']]
    person_movie = person_movie[person_movie['imdb_name_id'] != current_person]
    person_movie['person'] = current_person

    return person_movie