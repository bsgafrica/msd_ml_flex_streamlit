# -*- coding: utf-8 -*-
"""
Created on Fri Oct 21 08:12:47 2021

@author: Ryan.Julyan
"""
import pandas as pd 
import numpy as np

# movies = pd.read_csv('data/IMDb movies.csv')
# movies_small = movies[['imdb_title_id','title','year','budget','duration','writer'
#                         ,'actors','avg_vote','votes','country','language','genre'
#                         ,'date_published']]


# #working with good volume of votes 
# movies_small = movies_small[movies_small['votes'] > 200]

# #####Define target - avg votes >=7 (better balance than 8)
# movies_small['Target'] = np.where(
#     movies_small['avg_vote'] >= 7 , 1, 0)
# movies_small['Target'].value_counts()
# ### Class is imbalanced

# #drop actor col for now
# movies_small = movies_small.drop(columns ='actors')
# movies_small = movies_small.drop(columns ='title')

# ####Basic cleaning
# movies_small['year'] = pd.to_numeric(movies_small['year'], errors = 'coerce')

# #drop the budget col - too many nans
# movies_small = movies_small.drop(columns ='budget')


################################################################################
##################### Median_best_vote by Month ################################
################################################################################

def median_best_vote(movies_small):
    movies_small['date_published'] =  pd.to_datetime(movies_small['date_published'], errors = 'coerce')
    
    movies_small['month'] = movies_small['date_published'].dt.month
    
    movies_by_month = movies_small[['date_published','avg_vote']]
    movies_by_month.index = movies_by_month['date_published']
    
    #get monthly best
    df_month = movies_by_month.resample('M').max()
    #group months and get median
    df_monthly_median = df_month.groupby(df_month.index.month).median()
    df_monthly_median['median_best_vote'] = df_monthly_median['avg_vote']
    df_monthly_median = df_monthly_median.drop(columns ='avg_vote')
    df_monthly_median.index.rename('month',inplace=True)
    
    df_monthly_median = df_monthly_median.reset_index()
    
    ## print(df_monthly_median)
    
    movies_small = movies_small.merge(df_monthly_median)
    
    return df_monthly_median

#monthly_median = median_best_vote(movies_small)

###########################################################################################
##################### Coefficient of variation (Ralative Delivation) by Genre #############
###########################################################################################

def coefficient_of_variation_by_genre(movies_small):

    movies_small_genres = pd.concat([movies_small['imdb_title_id'], movies_small['genre'].str.split(', ', expand=True)], axis=1)
    movies_small_genres['genre_0'] = movies_small_genres[0]
    movies_small_genres['genre_1'] = movies_small_genres[1]
    movies_small_genres['genre_2'] = movies_small_genres[2]
    movies_small_genres_0 = pd.concat([movies_small['imdb_title_id'],pd.get_dummies(movies_small_genres.genre_0, dtype=bool)], axis=1)
    movies_small_genres_1 = pd.concat([movies_small['imdb_title_id'],pd.get_dummies(movies_small_genres.genre_1, dtype=bool)], axis=1)
    movies_small_genres_2 = pd.concat([movies_small['imdb_title_id'],pd.get_dummies(movies_small_genres.genre_2, dtype=bool)], axis=1)
    
    left_cols = movies_small_genres_0.columns.tolist()
    right_cols = movies_small_genres_1.columns.tolist()
    
    
    left_cols = list(set(left_cols) & set(right_cols))
    right_cols = list(set(left_cols) & set(right_cols))
    
    movies_small_genres_merge_1 = pd.merge(movies_small_genres_0,movies_small_genres_1,how='left', left_on=left_cols, right_on = right_cols)
    
    left_cols = movies_small_genres_merge_1.columns.tolist()
    right_cols = movies_small_genres_2.columns.tolist()
    
    
    left_cols = list(set(left_cols) & set(right_cols))
    right_cols = list(set(left_cols) & set(right_cols))
    
    movies_small_genres_merge_2 = pd.merge(movies_small_genres_merge_1,movies_small_genres_2,how='left', left_on=left_cols, right_on = right_cols)
    
    cols = movies_small_genres_merge_2.columns
    
    cols = cols[1:]
    
    
    movies_small = movies_small.merge(movies_small_genres_merge_2)
    
    for col in cols:
        movies_small[col+'_mean_votes'] = movies_small['avg_vote'][movies_small[col] == True].mean()
        movies_small[col+'_std_votes'] = movies_small['avg_vote'][movies_small[col] == True].std()
        movies_small[col+'_cv_votes'] = (movies_small[col+'_std_votes'] / movies_small[col+'_mean_votes'])
        movies_small = movies_small.drop(columns = col+'_std_votes')
    
    return movies_small

#movies_small = coefficient_of_variation_by_genre(movies_small)


def coefficient_of_variation_by_genre_edited(movies_small):

    movies_small_genres = pd.concat([movies_small['imdb_title_id'], movies_small['genre'].str.split(', ', expand=True)], axis=1)
    movies_small_genres['genre_0'] = movies_small_genres[0]
    movies_small_genres['genre_1'] = movies_small_genres[1]
    movies_small_genres['genre_2'] = movies_small_genres[2]
    movies_small_genres = movies_small_genres.drop(columns={0,1,2})
    
    movies_small_genres = movies_small_genres.melt(id_vars=['imdb_title_id'], var_name='genre')
    movies_small_genres = movies_small_genres[['imdb_title_id','value']]
    movies_small_genres = movies_small_genres.rename(columns={'value':'genre'})

    movies_small_genres = pd.merge(movies_small_genres,
                                   movies_small[['imdb_title_id','avg_vote']]
                                   ,how = 'left'
                                   ,on = 'imdb_title_id')

    movies_small_genres_group = movies_small_genres.groupby('genre').agg({
        'avg_vote':['mean','std']})

    movies_small_genres_group.columns = ['_'.join(col).strip() for col in movies_small_genres_group.columns.values]
    movies_small_genres_group['genre_cv'] = movies_small_genres_group['avg_vote_std'] / movies_small_genres_group['avg_vote_mean']
    movies_small_genres_group =  movies_small_genres_group.fillna(0)
    movies_small_genres_group = movies_small_genres_group.rename(columns = {
        'avg_vote_mean': 'genre_avg_vote_mean'
        ,'avg_vote_std': 'genre_avg_vote_std'})

    return movies_small_genres_group

def apply_coefficient_of_variation_by_genre(movies_small,movies_small_genres_group):
    movies_small_genres = pd.concat([movies_small['imdb_title_id'], movies_small['genre'].str.split(', ', expand=True)], axis=1)
    try:
        movies_small_genres['genre_0'] = movies_small_genres[0]
    except:
        pass
    
    try:
        movies_small_genres['genre_1'] = movies_small_genres[1]
    except:
        pass
    try:
        movies_small_genres['genre_2'] = movies_small_genres[2]
    except:
        pass
    
    try:
        movies_small_genres = movies_small_genres.drop(columns={0,1,2})
    except:
        try:
            movies_small_genres = movies_small_genres.drop(columns={0,1})
        except:
            try:
               movies_small_genres = movies_small_genres.drop(columns={0})
            except:
               pass
            
    movies_small_genres = movies_small_genres.melt(id_vars=['imdb_title_id'], var_name='genre')
    movies_small_genres = movies_small_genres[['imdb_title_id','value']]
    movies_small_genres = movies_small_genres.rename(columns={'value':'genre'})
    
    movie_genre_rating = pd.merge(movies_small_genres
                                  ,movies_small_genres_group
                                  ,how = 'left'
                                  ,on = 'genre')
    
    movie_genre_rating = movie_genre_rating.groupby('imdb_title_id').mean()
    
    return movie_genre_rating
