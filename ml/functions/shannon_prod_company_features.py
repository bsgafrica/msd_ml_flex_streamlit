# -*- coding: utf-8 -*-
"""
Created on Tue Oct 19 08:59:56 2021

@author: Shannon.Beahan

#FEATURES:
    Feature:        Popular production company
    Description:    Production company with average rating of >= 7
    Exclusion:      must have produced X amount of movies in the last year (I will determine X)
"""

import pandas as pd
import numpy as np



#-----------------------------------------------------------------------------
#IMPORT DATA
movies = pd.read_csv('data/IMDb movies.csv')
movies_small = movies[['imdb_title_id','production_company','title','year','budget','duration','writer'
                       ,'actors','avg_vote','votes','country','language','genre'
                       ,'date_published']]


#-----------------------------------------------------------------------------
#CLEAN
#working with good volume of votes 
movies_small = movies_small[movies_small['votes'] > 200]

#####Define target - avg votes >=7 (better balance than 8)
movies_small['Target'] = np.where(
    movies_small['avg_vote'] >= 6.5 , 1, 0)
movies_small['Target'].value_counts()
### Class is imbalanced

#drop actor col for now
movies_small = movies_small.drop(columns ='actors')
movies_small = movies_small.drop(columns ='title')

####Basic cleaning
movies_small['year'] = pd.to_numeric(movies_small['year'], errors = 'coerce')

#drop the budget col - too many nans
movies_small = movies_small.drop(columns ='budget')







#-----------------------------------------------------------------------------
#FEATURE: Relevant Production Company
    
def feature_relevant_prod(df, movie_id_col, avg_vote_col, prod_col, year_col, year_val = 2015.0, movie_count = 10, avg_vote_val = 6.5):
    """
    FEATURE: RELEVANT PROD COMPANIES
    identifies relevant and popular prod companies by the following criteria:
    - made at least 10 movies since 2015
    - average vote of at least 6.5
    
    df:             dataframe to return feature on (dataframe)
    movie_id_col:   movie ID column (string)
    avg_vote_col:   avg vote column (string)
    prod_col:       production company column (string)
    year_col:       year movie came out column (string)
    year_val:       value to check year against, default 2015 (float)
    movie_count:    value to check count of movies against, default 10 (int)
    avg_vote_val:   value to check avg vote of last N movies against, default 6.5 (float)
    """
    
    #instantiate temp df
    temp = df.copy()
    
    #filter where year >+ year value
    temp = temp[temp[year_col] >= year_val]
    
    #get mean votes and count of movies for production companies
    temp = temp.groupby([prod_col]).agg(
    {
     movie_id_col: 'count',
     avg_vote_col: 'mean'
     }).reset_index()
    
    #filter for top movies by movie count
    temp = temp[temp[movie_id_col] >= movie_count]
    #filter for top movies by avg vote
    temp = temp[temp[avg_vote_col] >= avg_vote_val]
    
    #temp
    
    #return feature on dataframe
    #df['feature_relevant_prod_comp'] = np.where(
    #df[prod_col].isin(temp[prod_col]), 1, 0)
    
    #df = df[['production_company','feature_relevant_prod_comp']]
    #df = df.drop_duplicates(subset = 'production_company')
    
    return temp

#TEST:
test = feature_relevant_prod(movies_small, 'imdb_title_id', 'avg_vote', 'production_company', 'year', year_val = 2015, movie_count = 10, avg_vote_val = 6.5)




#-----------------------------------------------------------------------------
#FEATURE: Rising Popularity Production Company
    
def feature_rising_prod(df, movie_id_col, avg_vote_col, prod_col, year_col, year_val = 2015, movie_count = 10, avg_vote_val = 6.5):
    """
    FEATURE: RISING POPULAR PROD COMPANIES
    identifies rising popular prod companies by the following criteria:
    - made at least 10 movies since 2015
    - positive change in beginning avergae rating to current average rating (by 1st and last 3 movies)
    - avg vote of last 3 movies >= 6.5
    
    df:             dataframe to return feature on (dataframe)
    movie_id_col:   movie ID column (string)
    avg_vote_col:   avg vote column (string)
    prod_col:       production company column (string)
    year_col:       year movie came out column (string)
    year_val:       value to check year against, default 2015 (float)
    movie_count:    value to check count of movies against, default 10 (int)
    avg_vote_val:   value to check avg vote of last 3 movies against, default 6.5 (float)
    """

    #COMPARE avg rate FIRST THREE MOVIES TO avg rate LAST 3 MOVIES:
    #get avg for first 3 movies
    firstThreeMovies = df.groupby([prod_col], as_index=False).head(3)[[prod_col,avg_vote_col]]
    firstThreeMovies = firstThreeMovies.groupby(prod_col).mean().reset_index()
    #get avg for last 3 movies
    lastThreeMovies = df.groupby([prod_col], as_index=False).tail(3)[[prod_col,avg_vote_col]]
    lastThreeMovies = lastThreeMovies.groupby(prod_col).mean().reset_index()
    #merge on prod company
    upcoming_prod = pd.merge(firstThreeMovies,lastThreeMovies, how= 'left',on = prod_col)
    #calculate change in ratings
    upcoming_prod['change'] = upcoming_prod[(avg_vote_col+'_y')] - upcoming_prod[(avg_vote_col+'_x')]
    #identify with improvement by positive rating change
    upcoming_prod['improvement_indicator'] = np.where(upcoming_prod['change'] > 0,1,0)
    #filter for improving prod companies
    upcoming_prod = upcoming_prod[upcoming_prod['improvement_indicator']==1]
    #filter for prod companies with latest avg rating >= 6.5
    upcoming_prod = upcoming_prod[upcoming_prod[(avg_vote_col+'_y')] >= avg_vote_val]
    
    
    #re-instantiate temp df
    temp = df.copy()
    #reduce temp by upcoming_prod
    temp['is_rising'] =  np.where(
    temp[prod_col].isin(upcoming_prod[prod_col]), 1, 0)
    temp = temp[temp['is_rising'] == 1]
    
    #filter where year >= year value
    temp = temp[temp[year_col] >= year_val]
    #get mean votes and count of movies for production companies
    temp = temp.groupby([prod_col]).agg(
    {
     movie_id_col: 'count',
     }).reset_index()
    
    #filter for top movies by movie count
    temp = temp[temp[movie_id_col] >= movie_count]
        
    #return feature on dataframe
    #df['feature_rising_prod_comp'] = np.where(
    #df[prod_col].isin(temp[prod_col]), 1, 0)
    
    #df = df[['production_company','feature_relevant_prod_comp']]
    #df = df.drop_duplicates(subset = 'production_company')
    
    return temp

#TEST:
test2 = feature_rising_prod(movies_small, 'imdb_title_id', 'avg_vote', 'production_company', 'year', year_val = 2015.0, movie_count = 10, avg_vote_val = 6.5)















#-----------------------------------------------------------------------------
#-----------------------------------------------------------------------------
#-----------------------------------------------------------------------------
#-----------------------------------------------------------------------------
# #ANALYSIS
# #how many movies did each production company produce? 
# all_movies = movies_small['production_company'].value_counts()

# #how many movies did each production company produce in the last 5 years?
# #NB: not enough movies per production house in last year, rather last 5 years
# movies_small_2015 = movies_small[movies_small['year'] >= 2015]
# all_movies_2015 = movies_small_2015['production_company'].value_counts()



# #prod that have made more that 10 films in the last 5 years (avg 2 films a year)
# active_prods = movies_small_2015.groupby(['production_company']).agg(
#     {
#      'imdb_title_id': 'count',
#      #get the average ratings per film
#      'avg_vote': 'mean'
#      }).reset_index()
# #take the greater than 10
# active_prods = active_prods[active_prods['imdb_title_id']>=10]
# #add indicator column
# active_prods.insert(2, 'active_flag', 1)

# active_prods['rate_flag'] = np.where(
#     active_prods['avg_vote'] >= 6.5 , 1, 0)


# prods_list = active_prods[active_prods['rate_flag']==1]

# movies['feat'] = np.where(
#     movies['production_company'].isin(prods_list['production_company']), 1, 0)




    

#JAYS CODE:

# #COMPARE avg rate FIRST THREE MOVIES TO avg rate LAST 3 MOVIES:
# firstThreeMovies = title_principal.groupby(['imdb_name_id'], as_index=False).head(3)[['imdb_name_id','avg_vote']]
# firstThreeMovies = firstThreeMovies.groupby('imdb_name_id').mean().reset_index()

# lastThreeMovies = title_principal.groupby(['imdb_name_id'], as_index=False).tail(3)[['imdb_name_id','avg_vote']]
# lastThreeMovies = lastThreeMovies.groupby('imdb_name_id').mean().reset_index()

# upcoming_actor = pd.merge(firstThreeMovies,lastThreeMovies, how= 'left',on = 'imdb_name_id')
# upcoming_actor['change'] = upcoming_actor['avg_vote_y'] - upcoming_actor['avg_vote_x']

# upcoming_actor['improvement_indicator'] = np.where(upcoming_actor['change'] > 0,
# 1,0)



# upcoming_actor = upcoming_actor[['imdb_name_id','improvement_indicator']]