# -*- coding: utf-8 -*-
"""
Created on Fri Oct 22 08:20:23 2021

@author: Jaymal.Kalidas
"""
import itertools
import pandas as pd

def get_person_relations(i, title_principal_poi_sample
                         ,temp_title_principal_name
                         ,temp_title_principal_movie):
    
    current_person = title_principal_poi_sample['imdb_name_id'][i]    
    person = temp_title_principal_name.loc[current_person]
    #step 2
    person_movie = temp_title_principal_movie.loc[person['imdb_title_id']]
    person_movie = person_movie[person_movie['imdb_name_id'] != current_person]
    person_movie['person'] = current_person
    print(i)

    return person_movie

result = []
results = []
def collect_result(result):
    #global results
    results.append(result)
    
    
    

def apply_person_movie_combinations(i, title_principal):
    
    current_person_id = title_principal['imdb_name_id'][i]   
    current_movie_id  = title_principal['imdb_title_id'][i]
    current_movie = title_principal[
        title_principal['imdb_title_id'] == title_principal['imdb_title_id'][i]]   
    current_movie_names = current_movie['imdb_name_id']

    name_combinations = list(itertools.combinations(current_movie_names, 2))
    name_combinations = pd.DataFrame(name_combinations)
    name_combinations['imdb_title_id'] = current_movie_id
    name_combinations = name_combinations.rename(columns={0: 'imdb_name_id',1:'person'})
    return name_combinations 