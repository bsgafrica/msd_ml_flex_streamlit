# -*- coding: utf-8 -*-
"""
Created on Thu Oct  7 11:23:07 2021

@author: Jaymal.Kalidas
"""

import pandas as pd
import numpy as np

#movies = pd.read_csv('data/IMDb movies.csv')
#movies_small = movies[['imdb_title_id','title','year','budget','duration','writer'
#                       ,'actors','avg_vote','votes','country','language','genre'
#                       ,'worlwide_gross_income']]



def reduce_title_principal_to_train_data(title_principal_path, movies_small, x_train):
    #names = pd.read_csv('data/IMDb names.csv')
    title_principal = pd.read_csv(title_principal_path)
    
    
    #working with good volume of votes 
    #movies_small = movies_small[movies_small['votes'] > 200]
    movie_ratings = movies_small[['imdb_title_id','avg_vote']]
    
    
    title_principal = pd.merge(title_principal, movie_ratings,
                               how = 'left',
                               on='imdb_title_id')
    
    #Remove nans
    title_principal = title_principal.dropna(subset=['avg_vote'])
    
    
    ####ONLY BUILD THE LOOKUP ON TRAINING DATA
    title_principal = title_principal[
        title_principal['imdb_title_id'].isin(x_train['imdb_title_id'])]
    
    return title_principal


########### IMPACT ACTOR FEATURE ###########
#For each actor get their avg score from train data and join to test data as well

def feature_impact_actor(title_principal_path, movies_small, x_train):
    title_principal = reduce_title_principal_to_train_data(title_principal_path,movies_small, x_train)
    
    title_principal_group = title_principal.groupby('imdb_name_id').agg(
        {
         'avg_vote':['mean','count']
         })
    
    
    title_principal_group.columns = ['_'.join(col).strip() for col in title_principal_group.columns.values]
    title_principal_group.dtypes
    
    #Actors with at least 5 movie ratings
    title_principal_group = title_principal_group[
        title_principal_group['avg_vote_count'] >= 5]
    
    vote_per_actor_dsit = title_principal_group['avg_vote_mean'].value_counts()
    #Average is 6
    title_principal_group['avg_vote_mean'].mean()
    
    title_principal_group['impact_actor'] =np.where(
        title_principal_group['avg_vote_mean'] >= 6.5, 'high',
        np.where(
        title_principal_group['avg_vote_mean'] >= 4, 'medium', 'low')
        )
    
    title_principal_group['impact_actor'].value_counts()
    
    #dummy encode variable
    title_principal_group = pd.get_dummies(title_principal_group, columns = ['impact_actor'])
    title_principal_group = title_principal_group.rename(columns = {'avg_vote_count':'experiance'})
    title_principal_group  = title_principal_group .reset_index()
    
    title_principal_group.to_csv('data/model/feature_impact_actor_group.csv')
    
    #Join back to title principal to include test data
    #Re-read in the full title principal to ensure test data is managed
    title_principal = pd.read_csv(title_principal_path)
    title_principal = pd.merge(title_principal,title_principal_group,
                               how = 'left',
                               on = 'imdb_name_id')
    
    
    title_principal = title_principal.dropna(subset=['experiance'])
    
    title_principal_final = title_principal.groupby('imdb_title_id').agg({
        'avg_vote_mean':'mean'
        ,'experiance' :'sum'
        ,'impact_actor_high':'sum'
        ,'impact_actor_low':'sum'
        ,'impact_actor_medium':'sum'    
        }
        ).reset_index()
    title_principal_final = title_principal_final.drop(columns = 'avg_vote_mean')

    return title_principal_final


def apply_impact_actor_feature(feature_impact_actor_group,title_principal_path):
    
    title_principal = pd.read_csv(title_principal_path)
    title_principal = pd.merge(title_principal,feature_impact_actor_group,
                           how = 'left',
                           on = 'imdb_name_id')
    title_principal = title_principal.groupby('imdb_title_id').agg({
        'avg_vote_mean':'mean'
        ,'experiance' :'sum'
        ,'impact_actor_high':'sum'
        ,'impact_actor_low':'sum'
        ,'impact_actor_medium':'sum'    
        }
        ).reset_index()
    return title_principal

########### IMPACT ACTOR FEATURE ###########



########### GROWING STAR FEATURE ###########
##For each actor from training data get their info

def feature_growing_star(title_principal_path, movies_small, x_train):
    
    ##RESET TITLE PRINCIPAL
    title_principal = reduce_title_principal_to_train_data('data/IMDb title_principals.csv',movies_small, x_train)
    
    
    firstThreeMovies = title_principal.groupby(['imdb_name_id'], as_index=False).head(3)[['imdb_name_id','avg_vote']]
    firstThreeMovies = firstThreeMovies.groupby('imdb_name_id').mean().reset_index()
    
    lastThreeMovies = title_principal.groupby(['imdb_name_id'], as_index=False).tail(3)[['imdb_name_id','avg_vote']]
    lastThreeMovies = lastThreeMovies.groupby('imdb_name_id').mean().reset_index()
    
    upcoming_actor = pd.merge(firstThreeMovies,lastThreeMovies, how= 'left',on = 'imdb_name_id')
    upcoming_actor['change'] = upcoming_actor['avg_vote_y'] -  upcoming_actor['avg_vote_x']
    
    upcoming_actor['improvement_indicator'] = np.where(upcoming_actor['change'] > 0,
                                                       1,0)
    
    upcoming_actor = upcoming_actor[['imdb_name_id','improvement_indicator']]
    
    upcoming_actor.to_csv('data/model/upcoming_actor.csv',index = False)
    
    #Join back to title principal to include test data
    title_principal = pd.read_csv('data/IMDb title_principals.csv')
    title_principal = pd.merge(title_principal,upcoming_actor,
                               how = 'left',
                               on = 'imdb_name_id')
    
    #Feature is at a movie level
    title_principal_improvement_indicator = title_principal.groupby('imdb_title_id').agg({
        'improvement_indicator':'sum'
        }).reset_index()
    
    return title_principal_improvement_indicator


def apply_feature_growing_star(title_principal_path, upcoming_actor):
    title_principal = pd.read_csv(title_principal_path)
    title_principal = pd.merge(title_principal,upcoming_actor,
                               how = 'left',
                               on = 'imdb_name_id')
    #Feature is at a movie level
    title_principal_improvement_indicator = title_principal.groupby('imdb_title_id').agg({
        'improvement_indicator':'sum'
        }).reset_index()
    
    return title_principal_improvement_indicator


########### GROWING STAR FEATURE ###########



########### ACTOR ACTRESS FEATURE ###########

# def feature_actor_actress_ratio(title_principal_path):
def feature_actor_actress_ratio(title_principal_path):
    ##RESET TITLE PRINCIPAL
    #Feature built on all the data
    #title_principal_path = 'data/IMDb title_principals.csv'
    title_principal = pd.read_csv(title_principal_path)
    
    actors_actress = title_principal
    actors_actress['actor']  = np.where(
        actors_actress['category'] == 'actor',1,0)
    actors_actress['actress']  = np.where(
        actors_actress['category'] == 'actress',1,0)
    
    actors_actress = actors_actress.groupby('imdb_title_id').agg({
        'actor':'sum',
        'actress':'sum'
        }).reset_index()
    actors_actress['actor_ratio'] = actors_actress['actor'] / actors_actress['actress']
    #set inf to 0
    actors_actress = actors_actress.replace([np.inf, -np.inf], 0)
    actors_actress['only_actors'] = np.where(actors_actress['actress'] == 0,1,0)
    actors_actress['only_actress'] = np.where(actors_actress['actor'] == 0,1,0)
    actors_actress = actors_actress[['imdb_title_id','actor_ratio','only_actors','only_actress']]
    
    return actors_actress

########### ACTOR ACTRESS FEATURE ###########


########### CAST SIZE FEATURE ###########
#Feature built on all the data
def feature_cast_size(title_principal_path):
    #title_principal_path = 'data/IMDb title_principals.csv'
    title_principal = pd.read_csv(title_principal_path)
    cast_size = title_principal.groupby('imdb_title_id').agg({
        'imdb_name_id':'count',
        }).reset_index()
    
    cast_size = cast_size.rename(columns = {'imdb_name_id':'cast_size'})
    
    return cast_size

########### CAST SIZE FEATURE ###########



####################Average rating per principal category####################
def calc_title_principal_category(title_principal_path,movies_small,x_train):
    #Read in title principal
    title_principal = pd.read_csv(title_principal_path)
    
    #Reduce to training data only
    title_principal = reduce_title_principal_to_train_data('data/IMDb title_principals.csv',movies_small, x_train)
    
    
    #Average rating per person on the movie
    title_principal_average_rating = title_principal.groupby('imdb_name_id').agg({
        'avg_vote':['mean','count'],
        }).reset_index()
    
    title_principal_average_rating.columns = [''.join(col).strip() for col in title_principal_average_rating.columns.values]
    
    ##Reduce the data to people that have done this over 5 times
    title_principal_average_rating = title_principal_average_rating[
        title_principal_average_rating['avg_votecount'] > 5]
    
    title_principal_average_rating = title_principal_average_rating.rename(
        columns={'avg_votemean':'avg_vote'})
    title_principal_average_rating = title_principal_average_rating[['imdb_name_id',
                                                                     'avg_vote']]
    
    title_principal_average_rating.to_csv('data/model/title_principal_average_rating.csv')
    
    ##Read in the full title principal including test data
    title_principal = pd.read_csv('data/IMDb title_principals.csv')
    #select the title, name and category
    title_principal = title_principal[['imdb_title_id','imdb_name_id','category']]
    #join trianing data votes
    title_principal  = pd.merge(title_principal,
                                title_principal_average_rating,
                                how = 'left',
                                on = 'imdb_name_id')
    
    title_principal = title_principal.fillna({'avg_vote':0})
    
    ## Group them in their categories per movie
    ## average per category
    title_principal_category_avg = title_principal.groupby(['imdb_title_id',
                                                           'category']).agg({
                                                               'avg_vote':'mean'
                                                               }).reset_index()


                                                           
    ##Remove unnessary roles that dont occur a lot
    title_principal_category_avg = title_principal_category_avg.pivot(
        index='imdb_title_id', columns='category', values='avg_vote').reset_index()
    
    title_principal_category_avg =title_principal_category_avg[[
    'imdb_title_id'    
    ,'actor'
    ,'director'             
    ,'actress'                
    ,'writer'                 
    ,'producer'               
    ,'composer'               
    ,'cinematographer']]
    
    title_principal_category_avg = title_principal_category_avg.fillna(0)
    
    return title_principal_category_avg


def apply_title_principal_category(title_principal_path, title_principal_average_rating):
    ##Read in the full title principal including test data
    #title_principal_path = 'data/IMDb title_principals.csv'
    title_principal = pd.read_csv(title_principal_path)
    #select the title, name and category
    title_principal = title_principal[['imdb_title_id','imdb_name_id','category']]
    #join trianing data votes
    title_principal  = pd.merge(title_principal,
                                title_principal_average_rating,
                                how = 'left',
                                on = 'imdb_name_id')
    
    title_principal = title_principal.fillna({'avg_vote':0})
    
    ## Group them in their categories per movie
    ## average per category
    title_principal_category_avg = title_principal.groupby(['imdb_title_id',
                                                           'category']).agg({
                                                               'avg_vote':'mean'
                                                               }).reset_index()
                                                               
    col_list = [    
    'actor'
    ,'director'             
    ,'actress'                
    ,'writer'                 
    ,'producer'               
    ,'composer'               
    ,'cinematographer']
    ##Fit specific structure
    title_principal_category_avg = title_principal_category_avg.pivot(
        index='imdb_title_id', columns='category', values='avg_vote').reindex(columns=col_list).reset_index()
    
    title_principal_category_avg = title_principal_category_avg.fillna(0)
    
    return title_principal_category_avg

####################Average rating per principal category####################



