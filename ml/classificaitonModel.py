# -*- coding: utf-8 -*-
"""
Created on Thu Oct  7 10:04:49 2021

@author: Jaymal.Kalidas
"""


import pandas as pd
pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)

import numpy as np

# data visualization
import seaborn as sns
%matplotlib inline
from matplotlib import pyplot as plt
from matplotlib import style

from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix
from sklearn import metrics

from sklearn.ensemble import RandomForestClassifier

from functions.cm_plot import plot_confusion_matrix
from functions.reduce_levels import reduce_levels_to_top_N

from functions.actors_feature import reduce_title_principal_to_train_data

from functions.actors_feature import feature_impact_actor
from functions.actors_feature import feature_growing_star
from functions.actors_feature import feature_actor_actress_ratio
from functions.actors_feature import feature_cast_size
from functions.actors_feature import calc_title_principal_category

from functions.shannon_prod_company_features import feature_relevant_prod
from functions.shannon_prod_company_features import feature_rising_prod


from functions.addtional_features import coefficient_of_variation_by_genre_edited
from functions.addtional_features import apply_coefficient_of_variation_by_genre
from functions.addtional_features import median_best_vote

from functions import topic_modelling_function as tp


##############################CLEAN DATA##############################
movies = pd.read_csv('data/IMDb movies.csv')
movies_small = movies[['imdb_title_id','title','year','budget','duration','writer'
                       ,'director','production_company'
                       ,'actors','avg_vote','votes','country','language','genre'
                       ,'date_published','description']]


#working with good volume of votes 
movies_small = movies_small[movies_small['votes'] > 200]

#####Define target - avg votes >=7 (better balance than 8)
movies_small['Target'] = np.where(
    movies_small['avg_vote'] >= 6.5 , 1, 0)
movies_small['Target'].value_counts()
### Class is imbalanced

#drop actor col for now
movies_small = movies_small.drop(columns ='actors')
movies_small = movies_small.drop(columns ='title')

####Basic cleaning
movies_small['year'] = pd.to_numeric(movies_small['year'], errors = 'coerce')

#drop the budget col - too many nans
movies_small = movies_small.drop(columns ='budget')

##############################Features##############################

movies_small['genre_combo'] = movies_small['genre']
movies_small = reduce_levels_to_top_N(movies_small, 'genre_combo', 20)
movies_small = reduce_levels_to_top_N(movies_small, 'writer', 20)
movies_small = reduce_levels_to_top_N(movies_small, 'director', 20)
#movies_small = reduce_levels_to_top_N(movies_small, 'production_company', 20)
#movies_small = reduce_levels_to_top_N(movies_small, 'actors', 100)
movies_small = reduce_levels_to_top_N(movies_small, 'country', 10)
movies_small = reduce_levels_to_top_N(movies_small, 'language', 10)

#month of the year released
movies_small['date_published'] = pd.to_datetime(movies_small['date_published'],errors ='coerce')
movies_small['month'] = movies_small['date_published'].dt.month
movies_small['day'] = movies_small['date_published'].dt.day
movies_small.dtypes

#label encode
features_to_encode = ['genre_combo','writer','director','country','language']
#genre
#production_company
#features_to_encode = ['genre','country','language']
#movies_small = pd.get_dummies(movies_small, columns = features_to_encode)


from sklearn.preprocessing import OneHotEncoder
ohe = OneHotEncoder(handle_unknown='ignore', sparse = False)
# Apply ohe on data
ohe.fit(movies_small[features_to_encode])
cat_ohe = ohe.transform(movies_small[features_to_encode])

#ohe.fit(movies_small['genre_combo'])
#cat_ohe = ohe.transform(movies_small['genre_combo'])

column_name = ohe.get_feature_names(['genre_combo','writer','director'
                                     ,'country','language'])
cat_ohe =  pd.DataFrame(cat_ohe, columns= column_name)

movies_small = pd.concat([movies_small,cat_ohe],axis=1)

from joblib import dump, load
dump(ohe, 'data/model/ohe.joblib') # save the model




#Features to build
##is part of sequal [second or third movie faeture]
##gripping word in title feature



#Drop nas
movies_small = movies_small.dropna()


#drop day col
#movies_small = movies_small.drop(columns = ['day','month'])
#movies_small = movies_small.drop(columns=['writer'])

######################Features built on all data###############
title_principal_path = 'data\IMDb title_principals.csv'
actor_actress = feature_actor_actress_ratio(title_principal_path)
movies_small = pd.merge(movies_small,actor_actress,
                        on='imdb_title_id'
                        ,how='left')

movies_small = movies_small.fillna({'actor_ratio':0
                                    ,'only_actors':0
                                    ,'only_actress':0})

cast_size = feature_cast_size(title_principal_path)
movies_small = pd.merge(movies_small,cast_size,
                    on='imdb_title_id'
                    ,how='left')

movies_small = movies_small.fillna({'cast_size':0})

######################Features built on all data###############







##############################TRAIN/TEST##############################




##Sort ascending the data by date
movies_small = movies_small.sort_values(by=['date_published'])

#Split train and test
x_train, x_test= np.split(movies_small, [int(.70 *len(movies_small))])


y_train = x_train['Target']
y_test = x_test['Target']

x_train = x_train.drop(columns=['Target'])
x_test = x_test.drop(columns=['Target'])

####Train test split
#y = movies_small['Target']
#x = movies_small.drop(columns = 'Target')
#x_train, x_test, y_train, y_test = train_test_split(x, y, test_size =0.3
#                                                    ,random_state = 123)



######################Features built on training data only###############
##Join Actors feature
title_principal_path = 'data\IMDb title_principals.csv'
title_principal_final = feature_impact_actor(title_principal_path, movies_small, x_train)
#title_principal_final.to_csv('data/model/feature_impact_actor.csv', index = False )
x_train = pd.merge(x_train,title_principal_final,
                        on='imdb_title_id'
                        ,how='left')

x_train = x_train.fillna({'experiance':0
                          ,'impact_actor_high':0
                          ,'impact_actor_medium':0
                          ,'impact_actor_low':0})

x_test = pd.merge(x_test,title_principal_final,
                        on='imdb_title_id'
                        ,how='left')
x_test = x_test.fillna({'experiance':0
                          ,'impact_actor_high':0
                          ,'impact_actor_medium':0
                          ,'impact_actor_low':0})

## join improvement indicator
title_principal_improvement_indicator = feature_growing_star(
    title_principal_path, movies_small, x_train)

#title_principal_improvement_indicator.to_csv('data/model/feature_growing_star.csv',index = False)

x_train = pd.merge(x_train,title_principal_improvement_indicator,
                        on='imdb_title_id'
                        ,how='left')

x_train = x_train.fillna({'improvement_indicator':0})

x_test = pd.merge(x_test,title_principal_improvement_indicator,
                        on='imdb_title_id'
                        ,how='left')
x_test = x_test.fillna({'improvement_indicator':0})


## join relationship feature
training_relationships = pd.read_csv('data/training_relationships.csv')
training_relationships = training_relationships[['imdb_title_id','relationship_strength']]
test_relationships = pd.read_csv('data/test_relationships.csv')
test_relationships = test_relationships[['imdb_title_id','relationship_strength']]


x_train = pd.merge(x_train,training_relationships,
                        on='imdb_title_id'
                        ,how='left')

x_train = x_train.fillna({'relationship_strength':0})

x_test = pd.merge(x_test,test_relationships,
                        on='imdb_title_id'
                        ,how='left')
x_test = x_test.fillna({'relationship_strength':0})



## join principal cat average
#calc manages train and test data within
title_principal_category_avg = calc_title_principal_category('data/IMDb title_principals.csv'
                                                             ,movies_small
                                                             ,x_train)

#title_principal_category_avg.to_csv('data/model/title_principal_category_avg.csv', index = False)
x_train = pd.merge(x_train,title_principal_category_avg,
                        on='imdb_title_id'
                        ,how='left')

x_test = pd.merge(x_test,title_principal_category_avg,
                        on='imdb_title_id'
                        ,how='left')




## Join prod company feature
relevant_prod_companies = feature_relevant_prod(x_train, 'imdb_title_id', 'avg_vote', 'production_company', 'year', year_val = 1905, movie_count = 3, avg_vote_val = 5.5)
relevant_prod_companies = relevant_prod_companies.rename(columns={'imdb_title_id':'prod_movie_count','avg_vote':'production_avg_vote'})
#relevant_prod_companies.to_csv('data/model/relevant_prod_companies.csv', index = False)

x_train = pd.merge(x_train,relevant_prod_companies,
                        on='production_company'
                        ,how='left')

x_test = pd.merge(x_test,relevant_prod_companies,
                        on='production_company'
                        ,how='left')

x_train = x_train.fillna({'prod_movie_count':0})
x_train = x_train.fillna({'production_avg_vote':0})
x_test = x_test.fillna({'prod_movie_count':0})
x_test = x_test.fillna({'production_avg_vote':0})


growing_prod_companies = feature_rising_prod(x_train, 'imdb_title_id', 'avg_vote', 'production_company', 'year', year_val = 1905.0, movie_count = 3, avg_vote_val = 5.5)
growing_prod_companies['feature_rising_prod_comp'] = 1
growing_prod_companies = growing_prod_companies[['production_company','feature_rising_prod_comp']]
#growing_prod_companies.to_csv('data/model/growing_prod_companies.csv', index = False)

x_train = pd.merge(x_train,growing_prod_companies,
                        on='production_company'
                        ,how='left')

x_test = pd.merge(x_test,growing_prod_companies,
                        on='production_company'
                        ,how='left')
x_train = x_train.fillna({'feature_rising_prod_comp':0})
x_test = x_test.fillna({'feature_rising_prod_comp':0})


x_train = x_train.drop(columns='production_company')
x_test = x_test.drop(columns='production_company')


##join genre feature
genre_ratings = coefficient_of_variation_by_genre_edited(x_train)
genre_ratings.to_csv('data/model/genre_ratings.csv', index = True)
movie_rating = apply_coefficient_of_variation_by_genre(x_train,genre_ratings)

x_train = pd.merge(x_train,movie_rating,
                        on='imdb_title_id'
                        ,how='left')

movie_rating_test = apply_coefficient_of_variation_by_genre(x_test,genre_ratings)

x_test = pd.merge(x_test,movie_rating_test,
                        on='imdb_title_id'
                        ,how='left')

##join month feature
monthly_median = median_best_vote(x_train)
#monthly_median.to_csv('data/model/monthly_median.csv')
x_train = pd.merge(x_train,monthly_median,
                        on='month'
                        ,how='left')
x_test = pd.merge(x_test,monthly_median,
                        on='month'
                        ,how='left')

x_train = x_train.drop(columns=['date_published'])
x_test = x_test.drop(columns=['date_published'])



#shouldnt train on the year as we split train test by year
x_train = x_train.drop(columns=('year'))
x_test = x_test.drop(columns=('year'))

################Topic modelling###############
# print(pd.Timestamp.now())
# df1,df2,df3,lda_model,dictionary = tp.topic_model(    
#     data=x_train,
#     text_col='description',
#     no_topics=15,
#     stem_lemmatize_function='lemmatize',
#     unique_identifier='imdb_title_id',
#     per_word_topics=True,
#     ngram_no=2)

########Save and load model
#df1.to_csv('data/tm/df1.csv')
#df2.to_csv('data/tm/df2.csv')
#df3.to_csv('data/tm/df3.csv')
df1 =pd.read_csv('data/tm/df1.csv')
#get only topics from df1
df1 = df1[['imdb_title_id','tokens','ngrams','0','1','2','3','4','5','6'
           ,'7','8','9','9','10','11','12','13','14','Best Topic'
           ,'Best Topic Probability']]
df2 =pd.read_csv('data/tm/df2.csv')
df3 =pd.read_csv('data/tm/df3.csv')

from gensim.test.utils import datapath
import gensim
# Save model to disk.
#lda_model.save('data/tm/model')
# Load a potentially pretrained model from disk.
lda_model = gensim.models.ldamodel.LdaModel.load('data/tm/model')

import pickle
from gensim.corpora import Dictionary
#pickle.dump(corpus, open('data/tm/corpus.pkl', 'wb'))
#dictionary.save('data/tm/dictionary.gensim')
dictionary = Dictionary.load('data/tm/dictionary.gensim')
########Save and load model

x_train = pd.merge(x_train,df1, how= 'left', on = 'imdb_title_id')

#x_test = x_test.head(100)
print(str(x_test.shape))
x_test['tokens'] = x_test['description'].apply(tp.tokenize)
x_test['tokens'] = x_test['tokens'].apply(tp.lemmatize)
x_test['ngrams'] = x_test['tokens'].apply(tp.make_ngrams, ngram_no=2)
print(str(x_test.shape))

#input = []
#input.append(x_test['ngrams'][45270])


data = x_test
print(str(data.shape))
unique_identifier = 'imdb_title_id'
id2word = dictionary
df_topics = pd.DataFrame()
for i in range(len(data)):
    
    #for i in range(50000,50005):
    if i % 100 == 0:
        print(f'Progress: {(i+1)} of {len(data)} records processed')
    text = data['ngrams'].iloc[i]
    unique_id = data[unique_identifier].iloc[i]
    
    bow = id2word.doc2bow(text)
    #must update this with the infer_topics
    #df = pd.DataFrame(lda_model.get_document_topics(bow)).transpose().tail(1)
    df = pd.DataFrame(lda_model.get_document_topics(bow),columns=['Topic','Prob']).transpose()
    df = pd.DataFrame(data=[list(df.iloc[1])],columns=list(df.iloc[0].astype(int)))
    
    #df[unique_identifier] = data[unique_identifier].iloc[i]
    df = df.assign(**{unique_identifier: unique_id})
    df_topics = pd.concat([df_topics,df],axis=0)
    
# Find most likely Topic and probability for each document
print('Apply topic probability filtering and finding top matches ...')
df_topics = df_topics.reset_index()
df_topics = df_topics.drop(columns=['index'])
df_topics['Best Topic'] = df_topics[df_topics.columns[~df_topics.columns.isin([unique_identifier,'Best Topic','Best Topic Probability'])]].idxmax(axis=1)
df_topics['Best Topic Probability'] = df_topics[df_topics.columns[~df_topics.columns.isin([unique_identifier,'Best Topic','Best Topic Probability'])]].max(axis=1)
 
print(str(data.shape))    
data = pd.merge(left=data, right=df_topics, how="left",on=unique_identifier)
print(str(data.shape))   
     
x_test = data    
################Topic modelling###############


## Fill all nas with zero
x_train = x_train.fillna(0)
x_test = x_test.fillna(0)


##Drop cols that make up the target and id fields
x_train = x_train.drop(columns =['votes','avg_vote','imdb_title_id'
                                 ,'description'
                                 ,'genre'
                                 #,'ngrams','tokens'
                                 ])
x_train = x_train.drop(columns =['ngrams','tokens'
                                 ])

x_test = x_test.drop(columns =['votes','avg_vote','imdb_title_id'
                               ,'description'
                               ,'genre'
                               #,'ngrams','tokens'
                               ])
x_test = x_test.drop(columns =['ngrams','tokens'
                               ])
#if training is re-read in
#x_train = x_train.drop(columns = ['Unnamed: 0']) 


######################Features built on training data only###############

####Save training data
# x_train.to_csv('data/training/x_train.csv')
# x_test.to_csv('data/training/x_test.csv')
# y_train.to_csv('data/training/y_train.csv')
# y_test.to_csv('data/training/y_test.csv')

x_train = pd.read_csv('data/training/x_train.csv')
x_test = pd.read_csv('data/training/x_test.csv')
y_train = pd.read_csv('data/training/y_train.csv')
y_test = pd.read_csv('data/training/y_test.csv')
y_train = y_train[['Target']]
y_test = y_test[['Target']]
x_train = x_train.drop(columns = ['Unnamed: 0']) 
x_train = x_train.drop(columns = ['9.1']) 
x_test = x_test.drop(columns = ['Unnamed: 0']) 

####Save training data





########################SELECT BEST FEATURES###########################
from sklearn.feature_selection import SelectKBest, chi2
# Get feature column list
cols_to_select = list(x_train.columns)
#cols_to_select.remove("imdb_title_id")
#cols_to_select.remove("description")

# select train columns
x_train = x_train[cols_to_select]

# instantiate SelectKBest to determine 20 best features
# Use chi2 test to determine correlation
best_features = SelectKBest(score_func=chi2, k=40)
fit = best_features.fit(x_train[cols_to_select], y_train)
df_scores = pd.DataFrame(fit.scores_)
df_columns = pd.DataFrame(x_train[cols_to_select].columns)
# concatenate dataframes
feature_scores = pd.concat([df_columns, df_scores], axis=1)
feature_scores.columns = ["Feature_Name", "Score"]  # name output columns
feature_scores = feature_scores.nlargest(40, "Score")
#feature_scores.to_csv('data/model/feature_scores.csv')
#print(feature_scores.nlargest(20, "Score"))  # print 20 best features

#hold_x_train = x_train
# Select important features (these were intuitive to me)
x_train = x_train[feature_scores['Feature_Name']]
x_test = x_test[feature_scores['Feature_Name']]
                  
########################SELECT BEST FEATURES###########################






##############################MODEL##############################

#Random forest
rf_classifier = RandomForestClassifier(
                      min_samples_leaf=30,
                      n_estimators=300,
                      bootstrap=True,
                      #oob_score=True,
                      #n_jobs=-1,
                      class_weight = 'balanced',
                      random_state=123,
                      max_features='auto')

#Train
rf_classifier.fit(x_train, y_train)

#Predict
y_pred = rf_classifier.predict(x_test)
y_pred_probs = rf_classifier.predict_proba(x_test)


##############################Accuracy##############################

#training score
rf_classifier.score(x_train, y_train)

cm = confusion_matrix(y_test, y_pred)

plot_confusion_matrix(cm  = cm, 
                      normalize    = False,
                      target_names = ['Below 6.5', '6.5 and above'],
                      title        = "Confusion Matrix")


##Accuracy stats
from sklearn.metrics import accuracy_score
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from sklearn.metrics import f1_score

print('accuracy: ' + str( metrics.accuracy_score(y_test, y_pred)))
print('precision_score: ' + str(precision_score(y_test, y_pred)))
print('recall_score: ' + str(recall_score(y_test, y_pred)))
print('f1_score: ' + str(f1_score(y_test, y_pred)))


from matplotlib import pyplot
# get importance
importance = rf_classifier.feature_importances_
importance_df = pd.DataFrame(importance)
importance_df = importance_df.set_index( x_train.columns).reset_index()
importance_df =importance_df.rename(columns={'index':'feature'})

#importance_df.to_csv('feature_importance.csv')

# summarize feature importance
#for i,v in enumerate(importance):
#	print('Feature: %0d, Score: %.5f' % (i,v))
# plot feature importance
#pyplot.bar([x for x in range(len(importance))], importance)
#pyplot.show()

################## Save the model######################## 
import pickle
#filename = 'data/model/final_rf_model.pkl'
#pickle.dump(rf_classifier, open(filename, 'wb'))
################## Save the model######################## 


###SVM
from sklearn.svm import SVC
svm = SVC(kernel='linear',C=0.5,random_state = 123)
svm.fit(x_train, y_train)
y_pred = svm.predict(x_test)
cm = confusion_matrix(y_test, y_pred)

from sklearn import model_selection
from sklearn.ensemble import BaggingClassifier
from sklearn.tree import DecisionTreeClassifier
# AdaBoost Classification
from sklearn.ensemble import AdaBoostClassifier
seed = 7
num_trees = 70
kfold = model_selection.KFold(n_splits=10, random_state=seed)
model = AdaBoostClassifier(n_estimators=num_trees, random_state=seed)
results = model_selection.cross_val_score(model, x_train, y_train, cv=kfold)
model.fit(x_train, y_train)
print(results.mean())
#Predict
y_pred = model.predict(x_test)
cm = confusion_matrix(y_test, y_pred)


# bagging classifier
from sklearn.ensemble import BaggingClassifier
kfold = model_selection.KFold(n_splits=10, random_state=7)
cart = DecisionTreeClassifier()
num_trees = 100
model = BaggingClassifier(base_estimator=cart, n_estimators=num_trees, random_state=7)
results = model_selection.cross_val_score(model, x_train, y_train, cv=kfold)
print(results.mean())

#adabosst classfier
from sklearn.ensemble import AdaBoostClassifier
seed = 7
num_trees = 70
kfold = model_selection.KFold(n_splits=10, random_state=seed)
model = AdaBoostClassifier(n_estimators=num_trees, random_state=seed)
results = model_selection.cross_val_score(model, x_train, y_train, cv=kfold)
print(results.mean())
##77.9%


##https://www.datacamp.com/community/tutorials/ensemble-learning-python
# Voting Ensemble for Classification

from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
from sklearn.svm import SVC
from sklearn.ensemble import VotingClassifier
from sklearn.ensemble import BaggingClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn import model_selection
seed = 123

kfold = model_selection.KFold(n_splits=4, random_state=seed)
# create the sub models
estimators = []
model1 = LogisticRegression()
estimators.append(('logistic', model1))
model2 = DecisionTreeClassifier()
estimators.append(('cart', model2))
model3 = BaggingClassifier(base_estimator=cart, n_estimators=num_trees, random_state=7)
#model3 = RandomForestClassifier
estimators.append(('bagging', model3))
#model3 = SVC()
#estimators.append(('svm', model3))
# create the ensemble model
ensemble = VotingClassifier(estimators)
results = model_selection.cross_val_score(ensemble,x_train, y_train, cv=kfold)
print(results.mean())


#https://www.analyticsvidhya.com/blog/2018/06/comprehensive-guide-for-ensemble-models/
####



##############################MODEL STACKING##############################
from sklearn import tree
from sklearn.model_selection import StratifiedKFold
from sklearn.neighbors import KNeighborsClassifier

from sklearn.linear_model import LogisticRegression

train_pred = []
test_pred = []
## Stacking
def Stacking(model,train,y,test,n_fold):
    
    
    folds=StratifiedKFold(n_splits=n_fold,random_state=1)
    test_pred=np.empty((0,1),float)
    train_pred=np.empty((0,1),float)
    
    #no cross validation
    model.fit(X=x_train,y=y_train)
    train_pred=np.append(train_pred,model.predict(x_train))
    test_pred=np.append(test_pred,model.predict(x_test))    
    
    # for train_indices,val_indices in folds.split(train,y.values):
        
    #     x_train,x_val=train.iloc[train_indices],train.iloc[val_indices]
    #     y_train,y_val=y.iloc[train_indices],y.iloc[val_indices]

    #     model.fit(X=x_train,y=y_train)
    #     train_pred=np.append(train_pred,model.predict(x_val))
    #     test_pred=np.append(test_pred,model.predict(test))
  
    return test_pred.reshape(-1,1),train_pred

model1 = tree.DecisionTreeClassifier(random_state=1)

test_pred1 ,train_pred1=Stacking(model=model1,n_fold=10, train=x_train,test=x_test,y=y_train)

train_pred1=pd.DataFrame(train_pred1)
test_pred1=pd.DataFrame(test_pred1)

cm1 = confusion_matrix(y_test, test_pred1)
print('accuracy: ' + str( metrics.accuracy_score(y_test, test_pred1)))
print('f1_score: ' + str(f1_score(y_test, test_pred1)))

model2 = KNeighborsClassifier()

test_pred2 ,train_pred2=Stacking(model=model2,n_fold=10,train=x_train,test=x_test,y=y_train)

train_pred2=pd.DataFrame(train_pred2)
test_pred2=pd.DataFrame(test_pred2)

cm2 = confusion_matrix(y_test, test_pred2)
print('accuracy: ' + str( metrics.accuracy_score(y_test, test_pred2)))
print('f1_score: ' + str(f1_score(y_test, test_pred2)))


#Random forest
model3 = RandomForestClassifier(
                      min_samples_leaf=5,
                      n_estimators=500,
                      bootstrap=True,
                      #oob_score=True,
                      #n_jobs=-1,
                      class_weight = 'balanced',
                      random_state=123,
                      max_features='auto')

test_pred3 ,train_pred3=Stacking(model=model3,n_fold=10,train=x_train,test=x_test,y=y_train)
train_pred3=pd.DataFrame(train_pred3)
test_pred3=pd.DataFrame(test_pred3)
cm3 = confusion_matrix(y_test, test_pred3)
print('accuracy: ' + str( metrics.accuracy_score(y_test, test_pred3)))
print('f1_score: ' + str(f1_score(y_test, test_pred3)))


#adabosst classfier
from sklearn.ensemble import AdaBoostClassifier
from sklearn import model_selection
seed = 7
num_trees = 70
kfold = model_selection.KFold(n_splits=10, random_state=seed)
model4 = AdaBoostClassifier(n_estimators=num_trees, random_state=seed)

test_pred4 ,train_pred4=Stacking(model=model4,n_fold=10,train=x_train,test=x_test,y=y_train)
train_pred4=pd.DataFrame(train_pred4)
test_pred4=pd.DataFrame(test_pred4)
test_pred3=pd.DataFrame(test_pred3)
print('accuracy: ' + str( metrics.accuracy_score(y_test, test_pred4)))
print('f1_score: ' + str(f1_score(y_test, test_pred4)))




from sklearn.ensemble import GradientBoostingClassifier
model5 = GradientBoostingClassifier(learning_rate=0.01,random_state=1)
test_pred5 ,train_pred5=Stacking(model=model5,n_fold=10,train=x_train,test=x_test,y=y_train)
train_pred5=pd.DataFrame(train_pred5)
test_pred5=pd.DataFrame(test_pred5)
print('accuracy: ' + str( metrics.accuracy_score(y_test, test_pred5)))
print('f1_score: ' + str(f1_score(y_test, test_pred5)))



import xgboost as xgb
model6=xgb.XGBClassifier(random_state=1,learning_rate=0.01)
test_pred6 ,train_pred6=Stacking(model=model6,n_fold=10,train=x_train,test=x_test,y=y_train)
train_pred6=pd.DataFrame(train_pred6)
test_pred6=pd.DataFrame(test_pred6)
print('accuracy: ' + str( metrics.accuracy_score(y_test, test_pred6)))
print('f1_score: ' + str(f1_score(y_test, test_pred6)))



from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import BaggingClassifier
cart = DecisionTreeClassifier()
num_trees = 100
model7 = BaggingClassifier(base_estimator=cart, n_estimators=num_trees, random_state=7)
test_pred7 ,train_pred7=Stacking(model=model7,n_fold=10,train=x_train,test=x_test,y=y_train)
train_pred7=pd.DataFrame(train_pred7)
test_pred7=pd.DataFrame(test_pred7)
print('accuracy: ' + str( metrics.accuracy_score(y_test, test_pred7)))
print('f1_score: ' + str(f1_score(y_test, test_pred7)))




###Final model

df = pd.concat([train_pred1, train_pred2, train_pred3, train_pred4, train_pred5, train_pred6, train_pred7], axis=1)
df_test = pd.concat([test_pred1, test_pred2, test_pred3, test_pred4, test_pred5, test_pred6, test_pred7], axis=1)

df = pd.concat([train_pred1, train_pred3, train_pred4,train_pred6,train_pred7], axis=1)
df_test = pd.concat([test_pred1, test_pred3, test_pred4, test_pred6, test_pred7], axis=1)

model = LogisticRegression(random_state=1)
model.fit(df,y_train)
model.score(df_test, y_test)

y_pred = model.predict(df_test)

cm_final= confusion_matrix(y_pred, y_test)
##Accuracy stats
from sklearn.metrics import accuracy_score
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from sklearn.metrics import f1_score

print('accuracy: ' + str( metrics.accuracy_score(y_test, y_pred)))
print('precision_score: ' + str(precision_score(y_test, y_pred)))
print('recall_score: ' + str(recall_score(y_test, y_pred)))
print('f1_score: ' + str(f1_score(y_test, y_pred)))

##############################MODEL STACKING##############################