# -*- coding: utf-8 -*-
"""
Created on Tue Oct 19 07:46:42 2021

@author: Jaymal.Kalidas
"""
movies = pd.read_csv('data/IMDb movies.csv')
movies_small = movies[['imdb_title_id','title','year','budget','duration','writer'
                       ,'actors','avg_vote','votes','country','language','genre'
                       ,'date_published']]


#working with good volume of votes 
movies_small = movies_small[movies_small['votes'] > 200]

#####Define target - avg votes >=7 (better balance than 8)
movies_small['Target'] = np.where(
    movies_small['avg_vote'] >= 7 , 1, 0)
movies_small['Target'].value_counts()
### Class is imbalanced

#drop actor col for now
movies_small = movies_small.drop(columns ='actors')
movies_small = movies_small.drop(columns ='title')

####Basic cleaning
movies_small['year'] = pd.to_numeric(movies_small['year'], errors = 'coerce')

#drop the budget col - too many nans
movies_small = movies_small.drop(columns ='budget')