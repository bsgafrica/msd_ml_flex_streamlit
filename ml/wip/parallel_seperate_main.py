# -*- coding: utf-8 -*-
"""
Created on Fri Oct 22 08:19:25 2021

@author: Jaymal.Kalidas
"""
#https://www.machinelearningplus.com/python/parallel-processing-python/
#https://stackoverflow.com/questions/41385708/multiprocessing-example-giving-attributeerror

from multiprocessing import Pool
import multiprocessing as mp
from functions import defsV2
import pandas as pd


if __name__ == '__main__':
    

    ####################People collaboration Feature####################
    #Create a counter of how many times these individuals have worked together
    
    title_principal = pd.read_csv('data/IMDb title_principals.csv')
    
    #Reduce to training data only
    title_principal = reduce_title_principal_to_train_data('data/IMDb title_principals.csv',movies_small, x_train)
    
    #Reduce people to those that have appeared more than 5 times
    title_principal_poi = title_principal.groupby('imdb_name_id').agg({
        'avg_vote':'count',
        }).reset_index()
    
    ##Reduce the data to people that have done this over 5 times
    title_principal_poi = title_principal_poi[
        title_principal_poi['avg_vote'] > 5]
    
    
    title_principal = title_principal[title_principal[
        'imdb_name_id'].isin(title_principal_poi['imdb_name_id'])]
    
    title_principal = title_principal[['imdb_title_id','imdb_name_id']]

    title_principal_poi_sample = title_principal_poi
    title_principal_poi_sample = title_principal_poi_sample#[0:5000]
    title_principal_poi_sample = title_principal_poi_sample.reset_index()
    
    
    temp_title_principal_name =title_principal.set_index('imdb_name_id')
  
    temp_title_principal_movie =title_principal.set_index('imdb_title_id')

    end = len(title_principal_poi_sample['imdb_name_id'])
    results = []
    #print(defsV2.f(1))
    pool = mp.Pool(mp.cpu_count())
    #with Pool(2) as p:
#        results = p.apply(defsV2.get_person_relations, args()range(end))

#    starttime = pd.Timestamp.now()    
#    results = [pool.apply(defsV2.get_person_relations,
#                          args=(i, title_principal_poi_sample
#                                ,temp_title_principal_name
#                                ,temp_title_principal_movie)) for i in range(end)]
#    endtime = pd.Timestamp.now()
#    print(str(endtime-starttime))    
        
    #print(p.map(defsV2.f, [1, 2, 3]))
    def collect_result(result):
        global results
        results.append(result)

    result = []
    results = []
    starttime = pd.Timestamp.now()       
    for i in range(end):
        if i % 100 == 0:
            print(i)
        pool.apply_async(defsV2.get_person_relations,
                          args=(i, title_principal_poi_sample
                                ,temp_title_principal_name
                                ,temp_title_principal_movie), callback=collect_result)
        

    pool.close()
    pool.join()
    endtime = pd.Timestamp.now()
    print(str(endtime-starttime))
    df = pd.concat(results)


df_relationship_feature = df.groupby(['imdb_name_id','person']).size()

df_relationship_feature = df_relationship_feature.reset_index()

df_relationship_feature = df_relationship_feature[df_relationship_feature[0] > 1]

df_relationship_feature.to_csv('data/relationship_feature.csv')
