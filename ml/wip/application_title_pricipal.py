# -*- coding: utf-8 -*-
"""
Created on Mon Oct 25 15:38:41 2021

@author: Jaymal.Kalidas
"""


if __name__ == '__main__':
    from functions import defsV2
    from functions.actors_feature import reduce_title_principal_to_train_data
    import pandas as pd
    import itertools
    
    ###Training data
    
    title_principal = pd.read_csv('data/IMDb title_principals.csv')

    #Reduce to training data only
    title_principal = reduce_title_principal_to_train_data('data/IMDb title_principals.csv',movies_small, x_train)
    
    #list_of_poi_actors
    
    df_relationship_feature = pd.read_csv('data/relationship_feature.csv')
    
    actor_poi = df_relationship_feature['imdb_name_id']
    actor_poi  = actor_poi.drop_duplicates()
    
    #Reduce data to people that we have relationship info about
    title_principal = title_principal[title_principal['imdb_name_id'].isin(actor_poi)]
    
    title_principal = title_principal[['imdb_title_id','imdb_name_id']]
    list_of_movies = title_principal['imdb_title_id']
    list_of_movies =list_of_movies.drop_duplicates()
    
    temp_title_principal_name =title_principal.set_index('imdb_name_id')
    temp_title_principal_movie =title_principal.set_index('imdb_title_id')
    
    title_principal = title_principal.reset_index()
    title_principal = title_principal[['imdb_title_id','imdb_name_id']]
    

    results = [] 
    end = len(title_principal['imdb_name_id'])
    

    
    def collect_result(result):
        global results
        results.append(result)
    
    #########Without parallel processing
    for i in range(end):
         if i % 100 == 0:
            print(i)
         results.append(defsV2.apply_person_movie_combinations(i, title_principal))
    #########Without parallel processing     
    results_df = pd.concat(results)  
    #results_df.to_csv('data/training_data_relationships.csv')

    #########With parallel processing
    import multiprocessing as mp
    pool = mp.Pool(mp.cpu_count())
    
    result = []
    results = []
    starttime = pd.Timestamp.now()          
    for i in range(end):
     
        if i % 100 == 0:
            print(i)
        pool.apply_async(defsV2.apply_person_movie_combinations
                         ,args=(i, title_principal)
                             , callback=collect_result)
                           
    pool.close()
    pool.join()
    endtime = pd.Timestamp.now()
    print(str(endtime-starttime))
    df = pd.concat(results)                   
    #########With parallel processing                       
                       
                       
  

    #### Test data

    #########Without parallel processing     
    results_df = pd.concat(results)  
    #results_df.to_csv('data/test_data_relationships.csv')
    