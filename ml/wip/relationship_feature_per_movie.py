# -*- coding: utf-8 -*-
"""
Created on Thu Oct 28 09:56:34 2021

@author: Jaymal.Kalidas
"""
import pandas as pd

relationship_feature = pd.read_csv('data/relationship_feature.csv')
training_relationships = pd.read_csv('data/training_data_relationships.csv')
test_relationships = pd.read_csv('data/test_data_relationships.csv')

#Training relationships
training_relationships =pd.merge(training_relationships
                                 ,relationship_feature
                                 ,how = 'left'
                                 ,on=['imdb_name_id','person'])

training_relationships = training_relationships[['imdb_title_id', 'imdb_name_id','person','0']]
sample = training_relationships.head(100)

training_relationships = training_relationships.groupby('imdb_title_id').agg({
    '0':'sum'})
training_relationships = training_relationships.rename(columns={'0':'relationship_strength'}).reset_index()
#training_relationships.to_csv('data/training_relationships.csv')



##Test relationships
test_relationships =pd.merge(test_relationships
                                 ,relationship_feature
                                 ,how = 'left'
                                 ,on=['imdb_name_id','person'])
test_relationships = test_relationships[['imdb_title_id', 'imdb_name_id','person','0']]
sample = test_relationships.head(100)

test_relationships = test_relationships.groupby('imdb_title_id').agg({
    '0':'sum'})
test_relationships = test_relationships.rename(columns={'0':'relationship_strength'}).reset_index()
#test_relationships.to_csv('data/test_relationships.csv')
