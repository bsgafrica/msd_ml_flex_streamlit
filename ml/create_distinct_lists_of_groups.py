# -*- coding: utf-8 -*-
"""
Created on Wed Nov 10 15:28:37 2021

@author: Jaymal.Kalidas
"""
all_data = pd.read_csv('ml/data/IMDb title_principals.csv')
all_data = all_data[['imdb_name_id','category']]
people_names = pd.read_csv('ml/data/IMDb names.csv')
people_names = people_names[['imdb_name_id','name']]
all_data = pd.merge(all_data,people_names
                    ,how='left'
                    ,on='imdb_name_id')

all_data = all_data.drop_duplicates()

list_of_category = all_data['category']
list_of_category = list_of_category.unique()

list_of_actors = all_data[all_data['category'] == 'actor']
list_of_actors.to_csv('ml/data/list_of_actors.csv')

list_of_actress = all_data[all_data['category'] == 'actress']
list_of_actress.to_csv('ml/data/list_of_actress.csv')

list_of_director = all_data[all_data['category'] == 'director']
list_of_director.to_csv('ml/data/list_of_director.csv')

list_of_producer = all_data[all_data['category'] == 'producer']
list_of_producer.to_csv('ml/data/list_of_producer.csv')

list_of_composer = all_data[all_data['category'] == 'composer']
list_of_composer.to_csv('ml/data/list_of_composer.csv')

list_of_cinematographer = all_data[all_data['category'] == 'cinematographer']
list_of_cinematographer.to_csv('ml/data/list_of_cinematographer.csv')

list_of_writer = all_data[all_data['category'] == 'writer']
list_of_writer.to_csv('ml/data/list_of_writer.csv')


all_data = pd.read_csv('ml/data/IMDb movies.csv')
list_of_prod_companies = all_data['production_company']
list_of_prod_companies.to_csv('ml/data/list_of_prod_companies.csv')
