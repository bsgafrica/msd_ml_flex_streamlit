# Requirements
* Download and install [Python](https://www.python.org/downloads/) (suggested [3.8.6](https://www.python.org/downloads/release/python-386/)) if you do not already have it installed.
    * Ensure `pip` is installed (pip should be installed already because it comes with the latest versions of python) in case it is not, please install it from here: https://pip.pypa.io/en/stable/installing/
        * To check if pip is installed, you can run the following command in your terminal
```shell
python -m pip --version

```

> **NOTE:** This documentation assumes that you are running `pip3` as `pip`, as such all instructions are written with `pip`. If you would like to make `pip3` run when you call `pip`, from your tereminal, you can create a symlink to `pip3` from `pip`:

### Linux / Mac
* Open new terminal
    * "Control + Option + Shift + T" to open the terminal
        * You may need to run "cmd" as administrator to run `pip install --upgrade pip`
            * To do this, you run the command as `sudo` eg: `sudo pip install --upgrade pip`
```shell
nano ~/.bash_profile

```
> In the file, paste the following:

```shell
alias pip='pip3'
alias python='python3'

```

> **NOTE:** You may need to remove python 2.7 on MacOS as it is pre-installed on my distributions this [like](https://newbedev.com/how-to-uninstall-python-2-7-on-a-mac-os-x-10-6-4) was very helpful to acheive this:

1) Remove the third-party Python 2.7 framework
```shell
sudo rm -rf /Library/Frameworks/Python.framework/Versions/2.7
```
2) Remove the Python 2.7 applications directory
```shell
sudo rm -rf "/Applications/Python 2.7"
```
3) Remove the symbolic links, in /usr/local/bin, that point to this Python version. See them using
```shell
ls -l /usr/local/bin | grep '../Library/Frameworks/Python.framework/Versions/2.7'
``` 
and then run the following command to remove all the links:
```shell
cd /usr/local/bin/
ls -l /usr/local/bin | grep '../Library/Frameworks/Python.framework/Versions/2.7' | awk '{print $9}' | tr -d @ | xargs rm
```

## Dependencies

> Once you have installed Python and `pip`, you will need to install a dependency for the Flask-BDA command line functions to run correctly:
```shell
pip install virtualenv

```

# Local Environment

> To create and develop a local application, we are using [virtualenv](https://pypi.org/project/virtualenv/). A tool for creating isolated virtual python environments.

## Windows
* Open new terminal
    * "Windows-Key + R" will show you the 'RUN' box
    * Type "cmd" to open the terminal
        * You may need to run "cmd" as administrator to run `pip install --upgrade pip`
            * To do this, you can search for "cmd" in your windows search
            * Right-click on the icon, and click on the "Run as administrator" option
                * You may need to navigate to your project when running as administrator `cd <Path To>/my_awesome_project`
```shell
cd <Path To>/my_awesome_project

pip install --upgrade pip
pip install virtualenv
virtualenv venv

venv\Scripts\activate

pip install --upgrade pip
pip install --no-cache-dir -r requirements.txt

```

## Linux / Mac
* Open new terminal
    * "Control + Option + Shift + T" to open the terminal
        * You may need to run "cmd" as administrator to run `pip install --upgrade pip`
            * To do this, you run the command as `sudo` eg: `sudo pip install --upgrade pip`
```shell
cd <Path To>/my_awesome_project

pip install --upgrade pip
pip install virtualenv
virtualenv venv

source venv/bin/activate

pip install --upgrade pip
pip install --no-cache-dir -r requirements.txt

```

# Getting the required NLTK data-sets
> From inside the venv, type `python` to activate the python instance

```shell
(venv) > python

```
> Then run the following inside the python interactive environment

```python
import nltk

nltk.download('stopwords')
nltk.download('punkt')

```


# Running Streamlit


```shell
streamlit run app.py

```
> This will automatically start a local server and run it in your browser



# Installing Additional Python Packages

> If you include additional python packages in your project, don't forget to run `pip freeze` from your terminal to ensure you get the correct packages for your deployments

```shell
pip freeze > requirements.txt

```